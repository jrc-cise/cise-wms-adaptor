= CISE Adaptor for WMS data
Alessandro La_Porta <Alessandro.LA-PORTA@ext.ec.europa.eu>
v2.0, 2022-04-28

:page-layout: docs
:imagesdir: assets/images
:homepage: https://github.com/jrc-cise/cise-wms-adaptor
:numbered:
:source-highlighter: Coderay coderay
ifndef::env-site[]
:toc: right
:idprefix:
:idseparator: -
//:icons: font
endif::[]
ifdef::env-github[]
:tip-caption: :bulb:
:note-caption: :information_source:
:important-caption: :heavy_exclamation_mark:
:caution-caption: :fire:
:warning-caption: :warning:
endif::[]
:source-language: bash

== Overview
The CISE Adaptor for WMS data is an application to proxy the Http GetCapabilities request from a WMS Client through the Cise network to a remote WMS Server.
More information about the WMS protocol can be found here https://www.ogc.org/standards/wms

== Functionality
- The Http GetCapabilities request will be transported using a PullRequest for Location document service.
- Putting on hold the http request until the answer will arrive
- Retrieving the xml capabilities using a http request to a WMS server
- Sending the xml capabilities using a PullResponse for Location document service
- Changing the url contained in the xml answer with a new one to use a reverse proxy

== Requirements
The application should preferably installed on a linux machine. Theoretically should work also in any Operative System supporting Java and having a bash shell support but it has only been tested in a GNU/Linux server.

The server should be installed with:

- JDK 11
- Maven 3.5+

The following JDK have been proved to build the cise-vms-adaptor properly:

- GraalVM https://www.graalvm.org/docs/getting-started/
- Amazon Corretto https://aws.amazon.com/it/corretto/
- AdoptOpenJDK https://adoptopenjdk.net/
- Zulu OpenJDK https://www.azul.com/downloads/zulu-community/?package=jdk
- Liberica JDK https://bell-sw.com/

== Preparing the software
Put the distribution package `cise-wms-adaptor-bin.tar.gz` into the machine where the adaptor should be installed, in a directory where the software should run (i.e. /home/<user>)

[source,bash]
----
...$ cd /my/installation/path
...$ tar xvfz cise-wms-adaptor.tar.gz
...$ cd cise-wms-adaptor-bin
----

== Running the WMS Adaptor
The vms-adaptor application can be started by using a script `adaptor` in the distribution root path. Using the command:

The command:
[source,bash]
----
...$ ./adaptor run
----

it will be possible to start vms-adaptor in foreground. While to run it in background is possible to launch it with:

[source,bash]
----
...$ ./adaptor start
----

.Next steps
NOTE: Please refer to the link:src/main/asciidoc/configuration.adoc[configuration chapter] to configure the adaptor with the parameters specific to the public administration where the vms-adaptor will be installed.

== Docker container

To use docker image, the following version of docker and docker-compose
are needed:

*docker version 19 at least* +
*docker-compose version 1.25 at least*

The name of the docker image is

*ec-jrc/cise-wms-adaptor:latest*

=== Install the docker image

Perform the following steps : 

* Build the project using maven:
`mvn clean package`. It will create the following files under the
`/target` folder: `cise-wms-adaptor-bin-<version>.tar.gz` `cise-wms-adaptor-<version>-runner.tar.gz`
* In the base folder of the project, run the script : `build_distribution.sh` -
it will create `cise-wms-adaptor-distribution.tar` file which is required for
the docker image.

This script will do the following :

* Using the `Dockerfile` build the `ec-jrc/cise-wms-adaptor:latest` image
* Create a tar file `cise-wms-adaptor-distribution.tar` containing:

** `cise-wms-adaptor-<version>-bin.tar.gz` containing the actual cise-wms-adaptor application
(extract it to use it directly)
** `docker_cise-wms-adaptor_latest.tar.gz` the portable docker image
** Sample directories. These directories can be mapped to the running containers
so that they can be manipulated outside the docker image/container
*** `config` directory with sample inputs ( `dummyKeystore.jk`,  `application.properties`, `authfile.txt`, `wmscredentials.txt`).
*** `logs` directory
*** `docker_install.sh`
script so that the docker image can be loaded to docker
*** `docker-compose.yml` configuration to use the container with
docker-compose
*** `README.adoc` the current opened file

* Extract the files from cise-wms-adaptor-distribution.tar using `tar -xvf
cise-wms-adaptor-distribution.tar`. Please note that you can copy the .tar file
in any location/machine on which you want, in order to run the
cise-wms-adaptor.
* Install the docker image by running the `./docker_install.sh` script -
this will install the docker images required
* Update the `config` directory by adding the proper .jks file (Java Key
Store file). The node administrator should create a specific participant
for the CISE WMS Adaptor and provide the corresponding jks file both for
client side and server side installation
* Update the `authfile.txt` and `wmscredentials.txt` with appropriate information (and **encrypt** them) as explained in the link:src/main/asciidoc/configuration.adoc[configuration chapter]
* Update the application.properties file accordingly
* Run the docker container. When you run the docker container, you
should be asked for the jks and for the private key password. There are
two ways on which you can run the container:

. Using the docker-compose. Please *NOTE* that this command runs only in
the foreground which means that closing the shell will also close the
containers. If needed, you can use a window-like utility (such as
`screen`) so the process will run in background.
+
`docker-compose run --service-ports --rm cise-wms-adaptor`

. Using the docker run command directly. In this case you have to manually map the ports
and the volume directories:
+
`docker run -it -p 8080:8080 --mount src="$(pwd)/config",target=/srv/cise-wms-adaptor/config,type=bind --mount src="$(pwd)/logs",target=/srv/cise-wms-adaptor/logs,type=bind ec-jrc/cise-wms-adaptor:latest`

Please *NOTE* that if you run the container using the docker run
command, after you enter the passwords, you can detach the execution by
pressing Ctrl+p and Ctrl+q

=== Set up Docker volumes

By default the process above will use the directories shown below and
extracted through the `cise-wms-adaptor-distribution.tar` :

[cols=",,",options="header",]
|=======================================================================
|Folder |Folder in Docker |Description
|`config` |/srv/cise-wms-adaptor/`config` |Configuration files and .jks files

|`logs` |/srv/cise-wms-adaptor/`logs` |Logs
|=======================================================================

