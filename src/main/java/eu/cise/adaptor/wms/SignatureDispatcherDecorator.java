package eu.cise.adaptor.wms;

import eu.cise.adaptor.wms.resources.helpers.HttpErrors;
import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.dispatcher.DispatcherException;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.signature.SignatureService;

public class SignatureDispatcherDecorator implements Dispatcher {

    private final Dispatcher dispatcher;
    private final SignatureService signatureService;
    private final AdaptorLogger logger;

    public SignatureDispatcherDecorator(Dispatcher dispatcher, SignatureService signatureService, AdaptorLogger logger) {
        this.dispatcher = dispatcher;
        this.signatureService = signatureService;
        this.logger = logger;
    }

    @Override
    public DispatchResult send(Message message, String address) {
        DispatchResult result;
        try {
            result = dispatcher.send(signatureService.sign(message), address);
        } catch (DispatcherException dispatcherException) {
            var cause = dispatcherException.getCause();
            if (cause != null) {
                var innerCause = cause.getCause();
                if (innerCause != null) {
                    cause = innerCause;
                }
            }
            logger.logDispatcherException(message.getMessageID(), cause != null ? cause : dispatcherException);

            if (cause instanceof java.net.SocketException) {
                result = new DispatchResult(HttpErrors.SOCKET_ERROR, dispatcherException.getMessage());
            } else {
                result = new DispatchResult(HttpErrors.SERVER_ERROR, dispatcherException.getMessage());
            }
        }
        return result;
    }
}
