package eu.cise.adaptor.wms.resources;

import eu.cise.adaptor.wms.domain.wmsclient.CapabilitiesRequestor;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;


@Path("/wms")
public class WmsCapabilitiesResource {

    private final CapabilitiesRequestor capabilitiesRequestor;

    public WmsCapabilitiesResource(CapabilitiesRequestor capabilitiesRequestor) {
        this.capabilitiesRequestor = capabilitiesRequestor;
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_XML)
    @Operation(
            summary = "REST Service Endpoint to request the Xml Capabilities for a remote WMS Server",
            description = "This endpoint is used to retrieve the Xml Capabilities from a remote Wms Server." +
                    "It is the equivalent of the WMS protocol HTTP GetCapabilities")
     @APIResponses(
            value = {
                    @APIResponse(
                            name = "Xml Capabilities retrieved",
                            responseCode = "200",
                            description = "The Xml capabilities document is returned",
                            content = @Content(mediaType = APPLICATION_XML)),
                    @APIResponse(
                            name = "No Authorization",
                            responseCode = "401",
                            description = "The requester credential are not valid"),
                    @APIResponse(
                            name = "Xml Capabilities not found",
                            responseCode = "404",
                            description = "Some problem have occurred at server side and wasn't possible to retrieve the Xml capabilities")
            })
    public Uni<Response> getCapabilitiesGET(@QueryParam("service") String service, @QueryParam("request") String request, @QueryParam("version") String version) {
        return capabilitiesRequestor.getCapabilities(service, request, version);
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_XML)
    @Operation(
            summary = "REST Service Endpoint to request the Xml Capabilities for a remote WMS Server",
            description = "This endpoint is used to retrieve the Xml Capabilities from a remote Wms Server." +
                    "It is the equivalent of the WMS protocol HTTP GetCapabilities")
    @APIResponses(
            value = {
                    @APIResponse(
                            name = "Xml Capabilities retrieved",
                            responseCode = "200",
                            description = "The Xml capabilities document is returned",
                            content = @Content(mediaType = APPLICATION_XML)),
                    @APIResponse(
                            name = "No Authorization",
                            responseCode = "401",
                            description = "The requester credential are not valid"),
                    @APIResponse(
                            name = "Xml Capabilities not found",
                            responseCode = "404",
                            description = "Some problem have occurred at server side and wasn't possible to retrieve the Xml capabilities")
            })
    public Uni<Response> getCapabilitiesPOST(String payload) {
        String service = "WMS";
        String request = "GetCapabilities";
        String version = "1.3.0";
        return capabilitiesRequestor.getCapabilities(service, request, version);
    }

}
