package eu.cise.adaptor.wms.resources.helpers;

public interface HttpErrors {

    int SOCKET_ERROR = 555;
    int SERVER_ERROR = 500;
}
