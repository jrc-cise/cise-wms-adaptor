package eu.cise.adaptor.wms.resources.helpers;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.HeaderParam;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class WmsClientParams {

    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";

    @HeaderParam(AUTHORIZATION_PROPERTY)
    private final String authorization;

    @QueryParam("service")
    private final String service;

    @QueryParam("request")
    private final String request;

    @QueryParam("version")
    private final String version;

    public WmsClientParams(String username, String password) {
        authorization = StringUtils.isEmpty(username) ? "" : AUTHENTICATION_SCHEME + " " +  Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
        service = "WMS";
        request = "GetCapabilities";
        version = "1.3.0";
    }

    public WmsClientParams() {
        authorization = "";
        service = "WMS";
        request = "GetCapabilities";
        version = "1.3.0";
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getService() {
        return service;
    }

    public String getRequest() {
        return request;
    }

    public String getVersion() {
        return version;
    }
}
