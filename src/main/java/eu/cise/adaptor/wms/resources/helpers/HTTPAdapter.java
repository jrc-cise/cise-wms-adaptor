package eu.cise.adaptor.wms.resources.helpers;

import eu.cise.node.exceptions.NodeException;
import eu.cise.node.submission.AcceptanceResult;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Adapts the adapter response to  REST protocol
 */
public class HTTPAdapter {

    public static Response toResponse(AcceptanceResult result) {
        switch (result.getResultCode()) {
            case SUCCESS:
                return buildXmlResponse(result, 202);
            case AUTHENTICATION_ERROR:
            case BAD_REQUEST:
            case INVALID_REQUEST_OBJECT:
            case SERVICE_MANAGER_ERROR:
            case SERVER_ERROR:
            case UNEXPECTED_ERROR:
                return buildXmlResponse(result, 200);
            default:
                throw new NodeException("Not Supported response");
        }
    }

    private static Response buildXmlResponse(AcceptanceResult result, int status) {
        return Response.status(status)
                .type(MediaType.APPLICATION_XML)
                .entity(result.getResult())
                .build();
    }
}
