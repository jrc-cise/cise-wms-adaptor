package eu.cise.adaptor.wms.resources;

import eu.cise.adaptor.wms.resources.helpers.HTTPAdapter;
import eu.cise.node.submission.AcceptanceAgent;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;


@Path("/rest/api")
@OpenAPIDefinition(
        tags = {
                @Tag(name = "CISE", description = "Common Information Sharing Environment"),
                @Tag(name = "MessageResource", description = "Adaptor CISE message ingress")
        },
        info = @Info(
                title = "Adaptor CISE messages ingress API",
                version = "2.0.0",
                license = @License(
                        name = "Apache 2.0",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"))
)
public class MessagesResource {

    private final AcceptanceAgent ciseAcceptanceAgent;

    public MessagesResource(AcceptanceAgent ciseAcceptanceAgent) {
        this.ciseAcceptanceAgent = ciseAcceptanceAgent;
    }


    /**
     * This is the entry point of CISE message received from a CISE Node.
     *
     * @param payload the POST payload to be processed
     * @return the RESTeasy response
     */
    @POST
    @Consumes(APPLICATION_XML)
    @Produces(APPLICATION_XML)
    @Path("/messages")
    @PermitAll
    @Operation(
            summary = "REST Service Endpoint to accept CISE Messages",
            description = "The message resource allow the client to receive CISE messages from CISE network")
    @RequestBody(
            description = "The CISE Message is an xml document that complies to the CISE Data Model XSDs",
            required = true)
    @APIResponses(
            value = {
                    @APIResponse(
                            name = "Message accepted successfully",
                            responseCode = "202",
                            description = "Message accepted and in elaboration. A successful Sync Ack is returned",
                            content = @Content(mediaType = APPLICATION_XML)),
                    @APIResponse(
                            name = "Message elaboration caused an error",
                            responseCode = "200",
                            description = "Request terminated with an error. A Sync Ack with the error is returned",
                            content = @Content(mediaType = APPLICATION_XML))})

    public Response ciseMessage(String payload) {
        return HTTPAdapter.toResponse(ciseAcceptanceAgent.accept(payload));
    }
}
