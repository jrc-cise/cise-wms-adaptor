package eu.cise.adaptor.wms.wmsserver;

import org.eclipse.microprofile.config.spi.ConfigProviderResolver;
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;

import javax.inject.Singleton;
import javax.ws.rs.core.MultivaluedMap;

@Singleton
public class AddCustomHeaderParameter implements ClientHeadersFactory {


    private static String getParam(String propertyName) {
        return ConfigProviderResolver.instance().getConfig().getOptionalValue(propertyName, String.class).orElse("");
    }

    @Override
    public MultivaluedMap<String, String> update(MultivaluedMap<String, String> incomingHeaders, MultivaluedMap<String, String> clientOutgoingHeaders) {
        String headerParamKey = getParam("wms-api/mp-rest/header-param-key");
        String headerParamValue = getParam("wms-api/mp-rest/header-param-value");
        if (!headerParamKey.isBlank() && !headerParamValue.isBlank()) {
            clientOutgoingHeaders.add(headerParamKey, headerParamValue);
        }
        return clientOutgoingHeaders;
    }

}
