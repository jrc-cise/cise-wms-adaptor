package eu.cise.adaptor.wms.wmsserver;

import eu.cise.adaptor.wms.resources.helpers.WmsClientParams;
import eu.cise.adaptor.wms.security.ServerWmsCredentialRepository;
import io.quarkus.cache.CacheResult;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;

@ApplicationScoped
public class WmsCapabilitiesServer {

    private final WmsClient wmsClient;
    private final WmsClientParams wmsClientParams;

    public WmsCapabilitiesServer(@RestClient WmsClient wmsClient, ServerWmsCredentialRepository credentials) {
        this.wmsClient = wmsClient;
        this.wmsClientParams = credentials.isPermitAll() ?
                new WmsClientParams() :
                new WmsClientParams(credentials.getWmsCredential().getLeft(), credentials.getWmsCredential().getRight());
    }

    @CacheResult(cacheName = "capability-cache")
    public String getCapabilities() {

        try {
            return wmsClient.getCapabilities(wmsClientParams);
        } catch (WebApplicationException httpError) {
            manageRuntimeException(httpError);
            return null;
        } catch (RuntimeException ex) {
            manageRuntimeException(ex);
            return null;
        }
    }

    private void manageRuntimeException(WebApplicationException httpError) {
        throw new WmsServerException(httpError);
    }

    private void manageRuntimeException(RuntimeException ex) {
        // Http code 4xx or 5xx. (404 is not found)
        var cause = ex.getCause();
        if (cause == null) {
            throw new WmsServerException(ex);
        } else if (cause.getClass().getName().contains("java.net")) {
            throw new WmsServerException(cause);
        } else {
            throw new WmsServerException("ProcessingException not a network error: " + ex.getMessage(), cause);
        }
    }
}
