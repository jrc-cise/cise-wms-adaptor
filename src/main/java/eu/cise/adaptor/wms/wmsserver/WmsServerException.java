package eu.cise.adaptor.wms.wmsserver;


import javax.ws.rs.WebApplicationException;

public class WmsServerException extends RuntimeException {

    private final int httpCode;

    public WmsServerException(Throwable cause) {
        super(cause);
        httpCode = 0;
    }

    public WmsServerException(String message, Throwable cause) {
        super(message, cause);
        httpCode = 0;
    }

    public WmsServerException(String message) {
        super(message);
        httpCode = 0;
    }

    public WmsServerException(WebApplicationException webApplicationException) {
        super(webApplicationException.getMessage());
        httpCode = webApplicationException.getResponse().getStatus();
    }

    public int getHttpCode() {
        return httpCode;
    }
}
