package eu.cise.adaptor.wms.wmsserver;

import eu.cise.adaptor.wms.resources.helpers.WmsClientParams;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Singleton
@RegisterRestClient(configKey = "wms-api")
@RegisterClientHeaders(AddCustomHeaderParameter.class)
public interface WmsClient {

    @GET
    @Produces(MediaType.APPLICATION_XML)
    String getCapabilities(@BeanParam WmsClientParams wmsClientParams);

}
