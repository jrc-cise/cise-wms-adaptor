package eu.cise.adaptor.wms;

import eu.cise.adaptor.wms.security.crypto.EncryptorAesGcmPassword;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EncryptFile {


    public static void createFile(String fileName) throws InvalidAlgorithmParameterException, IOException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, InvalidKeyException {

        var encCred = encriptBasicAuth(fileName);
        writeEncodedFile(fileName, encCred);
    }

    private static List<String> encriptBasicAuth(String fileName) throws FileNotFoundException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, InvalidKeyException {

        List<String> encriptedCredentials = new ArrayList<>();
        try (var scanner = new Scanner(new FileInputStream(fileName), StandardCharsets.UTF_8).useDelimiter("\n")) {
            while (scanner.hasNext()) {
                var line = scanner.next();
                if (StringUtils.isNotEmpty(line)) {
                    String encoded = EncryptorAesGcmPassword.encrypt(line);
                    encriptedCredentials.add(encoded);
                }
            }
        }
        return encriptedCredentials;
    }

    private static void writeEncodedFile(String fileName, List<String> encodedCredentials) throws IOException {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (String encodedAuth : encodedCredentials) {
                writer.write(encodedAuth);
                writer.newLine();
            }
        }
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Only the name of the file to be encrypted is needed");
            System.exit(1);
        }

        try {
            EncryptFile.createFile(args[0]);
            System.out.println("File encryption done");
        } catch (Exception e) {
          System.out.println("The encryption process failed because: " + e.getMessage());
        }
    }
}
