package eu.cise.adaptor.wms;

import eu.cise.adaptor.wms.config.CiseConfig;
import eu.cise.adaptor.wms.config.GetMapConfig;
import eu.cise.adaptor.wms.config.SecurityConfig;
import eu.cise.adaptor.wms.config.SignatureConfig;
import eu.cise.adaptor.wms.domain.CiseMessageManager;
import eu.cise.adaptor.wms.domain.messages.CiseMessageBuilder;
import eu.cise.adaptor.wms.domain.wmsclient.CapabilitiesRequestor;
import eu.cise.adaptor.wms.domain.wmsclient.CapabilitiesResponseHandler;
import eu.cise.adaptor.wms.domain.wmsclient.SyncAsyncCoordinator;
import eu.cise.adaptor.wms.domain.wmsserver.CapabilitiesRequestHandler;
import eu.cise.adaptor.wms.security.AuthRepository;
import eu.cise.adaptor.wms.security.BasicAuthFileRepository;
import eu.cise.adaptor.wms.security.BasicAuthFileWmsCredentialRepository;
import eu.cise.adaptor.wms.security.ServerWmsCredentialRepository;
import eu.cise.adaptor.wms.wmsserver.WmsCapabilitiesServer;
import eu.cise.dispatcher.DispatcherFactory;
import eu.cise.dispatcher.DispatcherType;
import eu.cise.node.submission.AcceptanceAgent;
import eu.cise.node.submission.DefaultAcceptanceAgent;
import eu.cise.node.submission.DefaultSubmissionAgent;
import eu.cise.node.validation.service.SignatureValidatorServiceImpl;
import eu.cise.node.validation.signature.SignatureValidator;
import eu.eucise.xml.DefaultXmlMapper;
import io.quarkus.runtime.Startup;
import io.smallrye.config.ConfigValidationException;
import io.smallrye.config.SmallRyeConfig;
import io.smallrye.config.SmallRyeConfigBuilder;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.ConfigValue;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static eu.cise.signature.SignatureServiceBuilder.newSignatureService;

/**
 * The ApplicationContext class instantiate all the concrete classes utilized
 * by this adaptor and wired them so to be used.
 * <p>
 * Here there is a short summary of the concrete classes responsibilities:
 * - WmsServerResource: WMS server resource
 * - Process configuration: The information contained on the application.properties file on resources folder
 * <p>
 * All this resource are needed to build the domain default implementation classes:
 * - Signature processor: signature handler for processing the signature element on the CISE message
 * - AdaptorLogger: logging for the application and the domain
 * - Validation Factory : validation of the incoming message
 * - Acceptance Agent : accept and validate the incoming cise messages
 * - CapabilitiesHttpRequest: accept wms http getCapabilities requests
 * - AuthRepository: wms client username and password repository for security purpose, for wms clients
 * - ServerWmsCredentialRepository: wms server username and password repository
 */
@ApplicationScoped
@Startup
public class AppContext {

    private final AcceptanceAgent acceptanceAgent;
    private final CapabilitiesRequestor capabilitiesRequestor;
    private final AuthRepository authRepository;
    private final ServerWmsCredentialRepository wmsCredentialRepository;

    // configuration beans
    private final GetMapConfig getMapConfig;
    private final CiseConfig ciseConfig;
    private final SecurityConfig securityConfig;
    private final SignatureConfig signatureConfig;


    private final AdaptorLogger adaptorLogger;

    public AppContext(WmsCapabilitiesServer wmsCapabilitiesServer) throws IOException {

        // create and validate configuration entries in application.properties
        List<Exception> configurationValidationExceptions = new ArrayList<>();
        getMapConfig = buildConfig(GetMapConfig.class, configurationValidationExceptions);
        signatureConfig = buildConfig(SignatureConfig.class, configurationValidationExceptions);
        ciseConfig = buildConfig(CiseConfig.class, configurationValidationExceptions);
        securityConfig = buildConfig(SecurityConfig.class, configurationValidationExceptions);
        // check the configuration of the WMS Client URL
        checkManualConfigs("wms-api/mp-rest/url", configurationValidationExceptions);
        // check quarkus related configuration
        checkManualConfigs("quarkus.http.port", configurationValidationExceptions);
        checkManualConfigs("quarkus.log.file.enable", configurationValidationExceptions);
        checkManualConfigs("quarkus.log.file.path", configurationValidationExceptions);
        checkManualConfigs("quarkus.log.file.format", configurationValidationExceptions);
        checkManualConfigs("quarkus.log.category.\"eu.cise.dispatcher\".level", configurationValidationExceptions);
        checkManualConfigs("quarkus.cache.caffeine.capability-cache.expire-after-write", configurationValidationExceptions);
        checkConfigurationValidationExceptions(configurationValidationExceptions);


        var xmlMapper = new DefaultXmlMapper.NotValidating();

        this.adaptorLogger = new AdaptorLogger(xmlMapper);

        /* Dispatcher */
        var signatureService = newSignatureService().withKeyStoreName(signatureConfig.keyStoreName()).withKeyStorePassword(signatureConfig.keyStorePassword()).withPrivateKeyAlias(signatureConfig.privateKeyAlias()).withPrivateKeyPassword(signatureConfig.privateKeyPassword()).build();

        var dispatcherFactory = new DispatcherFactory();
        var dispatcher = new SignatureDispatcherDecorator(dispatcherFactory.getDispatcher(DispatcherType.REST, xmlMapper), signatureService, adaptorLogger);

        /* Adaptor domain classes */
        var ciseMessageFactory = new CiseMessageBuilder(ciseConfig);
        var syncAsyncCoordinator = new SyncAsyncCoordinator(ciseConfig.waitCapabilitiesTimeout(), adaptorLogger);

        var ciseMessageManager = new CiseMessageManager(new CapabilitiesRequestHandler(ciseConfig.serviceIdResponse(), dispatcher, ciseConfig.nodeAddress(), ciseMessageFactory, wmsCapabilitiesServer, adaptorLogger), new CapabilitiesResponseHandler(ciseConfig.serviceType(), getMapConfig.nodeEndpointUrl(), syncAsyncCoordinator, adaptorLogger, ciseConfig.wmsGetCapabilitiesUrl()), adaptorLogger);

        this.capabilitiesRequestor = new CapabilitiesRequestor(dispatcher, ciseConfig.nodeAddress(), ciseMessageFactory, syncAsyncCoordinator, adaptorLogger);

        /* Cise Domain */
        var submissionAgent = new DefaultSubmissionAgent(ciseMessageManager, adaptorLogger);

        var validatorService = new SignatureValidatorServiceImpl(new SignatureValidator(signatureService));

        this.acceptanceAgent = new DefaultAcceptanceAgent(xmlMapper, submissionAgent, validatorService, adaptorLogger);

        /* Security */
        this.authRepository = new BasicAuthFileRepository(securityConfig.clientUserPasswordFile());
        adaptorLogger.logSecurityGisClientActivated(authRepository.isPermitAll());

        this.wmsCredentialRepository = new BasicAuthFileWmsCredentialRepository(securityConfig.serverWmsPasswordFile());
        adaptorLogger.logSecurityWmsServerActivated(wmsCredentialRepository.isPermitAll());

    }

    @Produces
    public AcceptanceAgent getAcceptanceAgent() {
        return acceptanceAgent;
    }

    @Produces
    public CapabilitiesRequestor getWmsRequestStrategy() {
        return capabilitiesRequestor;
    }

    @Produces
    public AuthRepository getAuthRepository() {
        return authRepository;
    }

    @Produces
    public AdaptorLogger getAdaptorLogger() {
        return adaptorLogger;
    }

    @Produces
    public ServerWmsCredentialRepository getWmsCredentialRepository() {
        return wmsCredentialRepository;
    }

    @Produces
    public GetMapConfig getMapConfig() {
        return getMapConfig;
    }

    @Produces
    public CiseConfig getCiseConfig() {
        return ciseConfig;
    }

    @Produces
    public SecurityConfig getSecurityConfig() {
        return securityConfig;
    }

    @Produces
    public SignatureConfig getSignatureConfig() {
        return signatureConfig;
    }


    private <T> T buildConfig(Class<T> configClass, List<Exception> validationExceptions) {
        try {
            SmallRyeConfig config = new SmallRyeConfigBuilder()
                    .withMapping(configClass)
                    .withSources(StreamSupport.stream(ConfigProvider.getConfig().getConfigSources().spliterator(), false).collect(Collectors.toList()))
                    .build();

            return config.getConfigMapping(configClass);
        } catch (Exception illegalStateException) {
            validationExceptions.add(illegalStateException);
        }
        return null;
    }

    private void checkManualConfigs(String configurationPropertyName, List<Exception> configurationValidationExceptions) {
        ConfigValue existingConfig = ConfigProvider.getConfig().getConfigValue(configurationPropertyName);
        if (existingConfig.getValue() == null) {
            configurationValidationExceptions.add(new Exception("Configuration for property: " + configurationPropertyName + " is required but it could not be found in any config source"));
        }
    }

    private void checkConfigurationValidationExceptions(List<Exception> validationExceptions) {
        StringBuilder stringBuilder = new StringBuilder("FATAL: Application failed to start\n");
        stringBuilder.append("Please revisit application.properties as there are configuration errors:\n");
        int currentProblem = 0;
        for (Exception illegalStateException : validationExceptions) {
            if (illegalStateException.getCause() instanceof ConfigValidationException) {
                ConfigValidationException ex = (ConfigValidationException) illegalStateException.getCause();
                for (int i = 0; i < ex.getProblemCount(); i++) {
                    currentProblem++;
                    stringBuilder.append(currentProblem).append(") ").append(ex.getProblem(i).getMessage()).append("\n");

                }
            } else {
                currentProblem++;
                stringBuilder.append(currentProblem).append(") ").append(illegalStateException.getMessage()).append("\n");
            }
        }
        // if there were validation exceptions, print output in System.err and die
        if (!validationExceptions.isEmpty()) {
            System.err.println(stringBuilder);
            System.exit(1);
        }
    }
}

