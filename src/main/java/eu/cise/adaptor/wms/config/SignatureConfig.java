package eu.cise.adaptor.wms.config;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "signature")
public interface SignatureConfig {

    String keyStoreName();

    String keyStorePassword();

    String privateKeyAlias();

    String privateKeyPassword();
}
