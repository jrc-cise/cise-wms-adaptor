package eu.cise.adaptor.wms.config;

import eu.cise.servicemodel.v1.service.ServiceType;
import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "cise")
public interface CiseConfig {

    String nodeAddress();

    long waitCapabilitiesTimeout();

    String serviceIdRequest();

    String serviceIdResponse();

    ServiceType serviceType();

    String wmsGetCapabilitiesUrl();
}
