package eu.cise.adaptor.wms.config;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "getmap")
public interface GetMapConfig {

    String nodeEndpointUrl();
}
