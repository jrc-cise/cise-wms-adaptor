package eu.cise.adaptor.wms.config;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "security")
public interface SecurityConfig {

    String clientUserPasswordFile();

    String serverWmsPasswordFile();

}
