package eu.cise.adaptor.wms;

import eu.cise.dispatcher.DispatchResult;
import eu.cise.node.submission.SubmissionAgentLogger;
import eu.cise.servicemodel.v1.message.AcknowledgementType;
import eu.cise.servicemodel.v1.message.Message;
import eu.eucise.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AdaptorLogger implements SubmissionAgentLogger {

    private final Logger logger = LoggerFactory.getLogger("wms-adaptor");
    private final XmlMapper xmlMapper;

    public AdaptorLogger(XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    @Override
    public void logMessageAccepted(Message message) {

        if (logger.isInfoEnabled()) {
            logger.info("Accepted a message with id#[{}]", message.getMessageID());
        }

        if (logger.isDebugEnabled()) {
            logger.debug(xmlMapper.toXML(message));
        }

    }

    @Override
    public void logMessageRefused(Message message, String xmlMessage, String msgId, String corrId, String ackCode, String ackDetail) {

        if (logger.isInfoEnabled()) {
            logger.info("Refused a message with id#[{}] corrId[{}] ackCode[{}] ackDetail[{}]",
                    safeId(message), corrId, ackCode, ackDetail);
        }
    }

    @Override
    public void logSecurityEvent(String messageId, String eventType, String severityCode) {
        logger.info("Message with MessageID: {} refused, eventType {}, severityCode {}", messageId, eventType, severityCode);
    }

    public void lodDispatchResult(DispatchResult dispatchResult) {

        if (dispatchResult.isOK() && dispatchResult.getResult().getAckCode() != AcknowledgementType.SUCCESS) {
            logger.warn("Dispatch error, Acknowledgement reports corrId# {},  {} - {}", dispatchResult.getResult().getCorrelationID(), dispatchResult.getResult().getAckCode(), dispatchResult.getResult().getAckDetail());
        } else if (!dispatchResult.isOK()) {
            logger.warn("Dispatch error, Http reports {} - {}", dispatchResult.getHttpCode(), dispatchResult.getHttpErrorMessage());
        } else {
            var ack = dispatchResult.getResult();
            logger.info("Dispatched cise message id[{}], ackCode[{}], detail [{}]", ack.getCorrelationID(), ack.getAckCode(), ack.getAckDetail());
        }
    }

    public void logSocketError(String messageId) {
        logger.warn("Socket error for message id[{}]", messageId);
    }

    public void logDispatcherException(String messageId, Throwable ex) {
        logger.warn("Dispatch exception for message id[{}], message[{}]", messageId, ex.getMessage(), (ex.getCause() != null ? ex.getCause() : ex));
    }

    public void logXmlUriConversion(Map<String, String> uriLogMap) {
        for (Map.Entry<String, String> entry : uriLogMap.entrySet()) {
            logger.info("Uri conversion: [{}] -> [{}]", entry.getKey(), entry.getValue());
        }
    }

    public void logXmlConversionError(Exception e) {
        logger.warn("Xml Conversion error ", e);
    }

    public void logStartMessageManagement(Message message) {
        logger.info("Start management {} corrId[{}] id[{}]", message.getClass().getSimpleName(), message.getCorrelationID(), message.getMessageID());
    }

    public void logEndMessageManagement(Message message) {
        logger.info("End management {} corrId[{}] id[{}]", message.getClass().getSimpleName(), message.getCorrelationID(), message.getMessageID());
    }

    /* Wms Server */

    public void logServerError(String messageId, String errMessage) {
        logger.warn("WMS Server error for PullRequest id[{}]: {}", messageId, errMessage);
    }

    /* Cise PullRequest */
    public void logWmsRequest(String service, String request, String version, String messageId) {
        logger.info("Received Http getCapabilities request: service[{}] request[{}] version [{}]. Created PullRequest id[{}]", service, request, version, messageId);
    }

    /* Cise PullResponse */

    public void logCiseResponse(String messageId, String corrId) {
        logger.info("Sent PullResponse corrId[{}], id[{}]", corrId, messageId);
    }

    public void logCiseResponseError(String messageId, String code, String detail) {
        logger.warn("PullResponse with error id[{}], code[{}], detail[{}]", messageId, code, detail);
    }

    public void logCiseResponseException(String messageId, RuntimeException re) {
        logger.warn("Exception on PullResponse management id[{}], message[{}]", messageId, re.getMessage(), re);
    }

    /* SyncAsyncCoordinator */

    public void logCoorinatorLinkCreation(String correlationId) {
        logger.info("Link created for corrId[{}]", correlationId);
    }

    public void logCoordinatorSuccess(String correlationId, String xml, long elapsedTime) {
        logger.info("Give success response to link corrId[{}], xmlSize[{}] elapsedTime[{}]ms", correlationId, xml.length(), elapsedTime);
    }

    public void logCoordinatorFail(String correlationId, String cause, long elapsedTime) {
        logger.warn("Give failure response to link corrId[{}], cause[{}] elapsedTime[{}]ms", correlationId, cause, elapsedTime);
    }

    /* Security */
    public void logSecureAccepted(String username, String host, String resource) {
        logger.info("AUTH OK For [{}] Host[{}] Resource[{}]", username, host, resource);
    }

    public void logSecureNotAccepted(String info, String host, String resource) {
        logger.warn("AUTH KO For [{}] Host[{}] Resource[{}]", info, host, resource);
    }

    public void logSecurityWmsServerActivated(boolean isPermitAll) {
        if (isPermitAll) {
            logger.warn("The Authentication for WMS server is deactivated");
        } else {
            logger.warn("The Basic Authentication for WMS server is activated");
        }
    }

    public void logSecurityGisClientActivated(boolean isPermitAll) {
        if (isPermitAll) {
            logger.warn("The Authentication for GisClient is deactivated");

        } else {
            logger.warn("The Basic Authentication for GIS Client is activated");
        }
    }

    private String safeId(Message message) {
        return message == null ? "UNKNOWN" : message.getMessageID();
    }
}
