package eu.cise.adaptor.wms.domain.messages;

public class MessageException extends RuntimeException {
    public MessageException(String message) {
        super(message);
    }

    public MessageException(String message, Throwable exception) {
        super(message, exception);
    }
}
