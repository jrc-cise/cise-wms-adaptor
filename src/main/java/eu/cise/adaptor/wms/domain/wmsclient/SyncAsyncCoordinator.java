package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.servicemodel.v1.message.ResponseCodeType;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Links the synchronous http get request for wms capabilities, with the asynchronous response with it
 */
public class SyncAsyncCoordinator {

    // Timeout for the coordination in seconds
    private final long waitTimeout;

    // Mapping between correlation id and the http request
    private final Map<String, ResponseLink> httpRequestMap = new HashMap<>();

    private final AdaptorLogger logger;

    /**
     * Class constructor
     *
     * @param waitTimeout Response waiting timeout in seconds
     * @param logger logger instance
     */
    public SyncAsyncCoordinator(long waitTimeout, AdaptorLogger logger) {
        this.waitTimeout = waitTimeout;
        this.logger = logger;
    }

    /**
     * Link creator between request and response, by their common correlationId value
     *
     * @param correlationId Request correlationId
     * @return
     */
    public ResponseLink createLink(String correlationId) {
        ResponseLink responseLink = new ResponseLink(correlationId);
        httpRequestMap.put(correlationId, responseLink);

        logger.logCoorinatorLinkCreation(correlationId);

        return responseLink;
    }

    /**
     * Give the capabilities response to the original http request using the correlationId of the response (that is the same of the request)
     *
     * @param correlationId correlation id of the response
     * @param responseCodeType response result code
     * @param xmlCapabilities wms capabilities
     */
    public void giveResponse(String correlationId, ResponseCodeType responseCodeType, Optional<String> xmlCapabilities) {

        var link = httpRequestMap.get(correlationId);
        if (link == null) {
            logger.logCoordinatorFail(correlationId, "No link found", 0);
            return;
        }

        long elapsedTime  = System.currentTimeMillis() - link.getStartTime();
        Response response;
        if (xmlCapabilities.isPresent() && responseCodeType == ResponseCodeType.SUCCESS) {
            logger.logCoordinatorSuccess(correlationId, xmlCapabilities.get(), elapsedTime);
            response = Response.status(200)
                    .type(MediaType.APPLICATION_XML)
                    .entity(xmlCapabilities.get())
                    .build();
        } else if (!xmlCapabilities.isPresent()) {
            logger.logCoordinatorFail(correlationId, "No xmlCapabilities found", elapsedTime);
            response = responseNotFound();
        } else if (ResponseCodeType.AUTHENTICATION_ERROR == responseCodeType) {
            logger.logCoordinatorFail(correlationId, "Authentication error on wms server side", elapsedTime);
            response = responseNotAuth();
        } else {
            logger.logCoordinatorFail(correlationId, "Response with error " + responseCodeType, elapsedTime);
            response = responseServerError();
        }

        // Calling giveResponse, the blocking method will return with the response
        link.giveResponse(response);
    }

    private Response responseNotFound() {
        return Response.status(404).build();
    }

    private Response responseNotAuth() {
        return Response.status(401).build();
    }

    private Response responseServerError() {
        return Response.status(503).build();
    }

    /**
     * Contains the blocking method to wait the response
     */
    public class ResponseLink {

        private final String correlationId;
        private final BlockingQueue<Response> responseQueue = new LinkedBlockingQueue<>(1);
        private final long startTime;

        ResponseLink(String correlationId) {
            this.correlationId = correlationId;
            this.startTime = System.currentTimeMillis();
        }

        /**
         * Blocking method that give the http response with the wms capabilities
         * This method will return when:
         *      - the method giveResponse it's called
         *      - the waitTimeout is fired
         * @return  http response with the xml capabilities.
         *          If the there isn't any xml to be returned, then a http with error code will be returned
         */
        public Response getResponse() {
            try {
                var response = responseQueue.poll(waitTimeout, TimeUnit.SECONDS);
                if (response == null) {
                    logger.logCoordinatorFail(correlationId, "Wait timeout fired", waitTimeout);
                }
                return response != null ? response : responseNotFound();
            } catch (InterruptedException e) {
                return  responseNotFound();
            }
        }

        private boolean giveResponse(Response response) {
            return responseQueue.offer(response);
        }

        public long getStartTime() {
            return startTime;
        }
    }
}
