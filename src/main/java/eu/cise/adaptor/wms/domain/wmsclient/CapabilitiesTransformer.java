package eu.cise.adaptor.wms.domain.wmsclient;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class CapabilitiesTransformer {

    private static final String ONLINE_ALL = "//OnlineResource";
    private static final String ONLINE_SERVICE = "//Service/OnlineResource";
    private static final String ONLINE_GETCAPABILITIES = "//Capability//GetCapabilities//OnlineResource";
    private static final String ONLINE_GETMAP = "//Capability//GetMap//OnlineResource";
    private static final String ONLINE_GETFEATURE = "//Capability//GetFeatureInfo//OnlineResource";

    private final Document xmlDocument;
    private final XPath xPath;

    public CapabilitiesTransformer(String xmlCapabilities) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        builder.setEntityResolver(new WmsEntityResolver());
        this.xmlDocument = builder.parse(new InputSource(new StringReader(xmlCapabilities)));
        this.xPath = XPathFactory.newInstance().newXPath();
    }

    public void buildCapabilities(URL wmsStreamUrl, String adaptorWmsServiceUrl) throws XPathExpressionException, MalformedURLException {

        // Keep the original Service/OnlineResource url
        var nodeServiceUrl = getServiceUrl();

        // Change all the OnlineResource Url
        var nodelist = (NodeList) xPath.compile(ONLINE_ALL).evaluate(xmlDocument, XPathConstants.NODESET);
        replace(nodelist, wmsStreamUrl);

        // Update the GetCapabilities OnlineResource to the adaptor url
        nodelist = (NodeList) xPath.compile(ONLINE_GETCAPABILITIES).evaluate(xmlDocument, XPathConstants.NODESET);
        replace(nodelist, adaptorWmsServiceUrl);

        // Restore the original Service/OnlineResource url
        setServiceUrl(nodeServiceUrl);
    }

    public String getXml() throws TransformerException {

        // Recreate the xml in string format
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    private void replace(NodeList onlineResourceNodes, URL newUrl) {
        replace(onlineResourceNodes, newUrl.toExternalForm());
    }

    private void replace(NodeList onlineResourceNodes, String newUrl) {

        // For each tag (Xml Node) change the xlink:href attribute value
        for (int n = onlineResourceNodes.getLength() - 1; n >= 0; n--) {

            Node onlineResourceTag = onlineResourceNodes.item(n);
            short nodeType = onlineResourceTag.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {

                Node xlinkAttribute = onlineResourceTag.getAttributes().getNamedItem("xlink:href");
                xlinkAttribute.setNodeValue(newUrl);
            }
        }
    }

    /**
     * A URL is not a valid WMS url, if contains a query string with the parameter 'service' that is not equals do WMS
     *
     * @param nodeUrl url to be checked
     * @return true if is not a valid WMS url
     */
    private boolean isNotWmsUrl(String nodeUrl) {
        String paramName = "service";
        String correctValue = "WMS";

        try {
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(nodeUrl), StandardCharsets.UTF_8);

            for (NameValuePair param : params) {
                if (paramName.equalsIgnoreCase(param.getName())) {
                    return !correctValue.equalsIgnoreCase(param.getValue());
                }
            }
        } catch (URISyntaxException e) {
            return false;
        }
        return false;
    }

    /**
     * Recursive function which find the Layer tag and remove it from its father
     *
     * @param node Layer node
     */
    private void removeLayer(Node node) {
        if (node.getParentNode() == null) {
            return;
        }

        if ("Layer".equals(node.getNodeName())) {
            node.getParentNode().removeChild(node);
        } else {
            removeLayer(node.getParentNode());
        }
    }



    private String getServiceUrl() throws XPathExpressionException {
        var nodeService = (Node) xPath.compile(ONLINE_SERVICE).evaluate(xmlDocument, XPathConstants.NODE);
        Node xlinkAttribute = nodeService.getAttributes().getNamedItem("xlink:href");
        return xlinkAttribute.getNodeValue();
    }

    private void setServiceUrl(String newServiceUrl) throws XPathExpressionException {
        var nodeService = (Node) xPath.compile(ONLINE_SERVICE).evaluate(xmlDocument, XPathConstants.NODE);
        Node xlinkAttribute = nodeService.getAttributes().getNamedItem("xlink:href");
        xlinkAttribute.setNodeValue(newServiceUrl);
    }


    static class WmsEntityResolver implements EntityResolver {

        @Override
        public InputSource resolveEntity(String publicId, String systemId)
                throws SAXException, IOException {

            // Remove the dtd reference (the url is contained in systemId)
            return new InputSource(new StringReader(""));
        }
    }
}

