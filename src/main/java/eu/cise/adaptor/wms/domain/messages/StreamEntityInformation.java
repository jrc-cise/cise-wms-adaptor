package eu.cise.adaptor.wms.domain.messages;

import eu.cise.datamodel.v1.entity.document.Stream;
import eu.cise.datamodel.v1.entity.document.StreamType;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

/**
 * Wms Information contained in the Stream Entity
 *
 * <Stream>
 * 	<Title>
 * 		Is hard coded to 'WMS Services'
 * 	</Title>
 * 	<StreamType>
 * 		Is hard coded to ImageMap (is a value coming from the datamodel)
 * 	</StreamType>
 * 	<StreamURI>
 * 		 This is an example:
 * 		 file://eu.green/GetMap/eu.green.proxyserver.document.pull.prov.maps
 * 	</StreamURI>
 *
 * 	<Metadata>
 * 		<Description>
 * 			Is hard coded to 'WMS Capabilities'
 * 		</Description>
 * 		<FileSchema>
 * 			The xml with the capabilities received from the Wms Server.
 * 		</FileSchema>
 * 	</Metadata>
 * </Stream>
 */
public class StreamEntityInformation implements WmsStreamingInformation {

    private static final StreamType PAYLOAD_STREAM_TYPE = StreamType.IMAGE_MAP;
    private static final String PAYLOAD_STREAM_TITLE = "WMS Services";


    private final String streamUri;
    private final MetadataEntityInformation metadata;

    /**
     *  Create the Stream Entity with streamUri and xmlCapabilities
     *
     * @param streamUri
     * @param xmlCapabilities
     */
    public StreamEntityInformation(String streamUri, String xmlCapabilities) {
        this.streamUri = streamUri;
        this.metadata = new MetadataEntityInformation(xmlCapabilities);
    }

    /**
     * Retrieve the Stream Entity from the PullResponse payload
     *
     * @param pullResponse
     */
    public StreamEntityInformation(PullResponse pullResponse) {

        Optional<Stream> optionalStream = getStream(pullResponse);
        if (optionalStream.isPresent()) {

            var stream = optionalStream.get();
            this.streamUri = stream.getStreamURI();
            this.metadata = new MetadataEntityInformation(stream.getMetadatas());

        } else {
           throw new MessageException("Stream not found");
        }
    }


    @Override
    public Optional<String> getXmlCapabilities() {
        return this.metadata.getXmlCapabilities();
    }

    @Override
    public Optional<String> getStreamUri() {
        return StringUtils.isNotEmpty(streamUri) ? Optional.of(streamUri) : Optional.empty();
    }

    @Override
    public Stream buildEntity() {

        var stream = new Stream();

        stream.setStreamType(PAYLOAD_STREAM_TYPE);
        stream.setTitle(PAYLOAD_STREAM_TITLE);
        stream.setStreamURI(streamUri);
        stream.getMetadatas().add(metadata.buildMetadata());

        return stream;
    }

    private Optional<Stream> getStream(PullResponse pullResponse) {
        return Optional.of(pullResponse.getPayload())
                .filter(XmlEntityPayload.class::isInstance)
                .map(i -> ((XmlEntityPayload) i).getAnies())
                .filter(CollectionUtils::isNotEmpty)
                .map(i -> i.get(0))
                .filter(Stream.class::isInstance)
                .map(Stream.class::cast);
    }

}
