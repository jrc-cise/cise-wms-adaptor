package eu.cise.adaptor.wms.domain.messages;

import eu.cise.adaptor.wms.config.CiseConfig;
import eu.cise.adaptor.wms.wmsserver.WmsServerException;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.LocationDocument;
import eu.cise.datamodel.v1.entity.document.Stream;
import eu.cise.servicemodel.v1.message.*;
import eu.cise.servicemodel.v1.service.Service;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.PullRequestBuilder;
import eu.eucise.helpers.PullResponseBuilder;
import eu.eucise.helpers.ServiceBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.UUID;


public class CiseMessageBuilder {

    private final String serviceIdRequest;
    private final String serviceIdResponse;
    private final ServiceType serviceType;

    public CiseMessageBuilder(CiseConfig ciseConfig) {
        this.serviceIdResponse = ciseConfig.serviceIdResponse().trim();
        this.serviceIdRequest = ciseConfig.serviceIdRequest().trim();
        this.serviceType = ciseConfig.serviceType();

        // Test the service type configured
        getEmptyEntity(serviceType);

    }

    public static WmsStreamingInformation getShomInformationEntity(ServiceType serviceType, PullResponse pullResponse) throws MessageException {
        switch (serviceType) {
            case LOCATION_DOCUMENT_SERVICE:
                return new LocationDocumentEntityInformation(pullResponse);
            case DOCUMENT_SERVICE:
                return new StreamEntityInformation(pullResponse);
        }
        throw throwServiceNotSupported(serviceType);
    }

    public static WmsStreamingInformation getShomInformationEntity(ServiceType serviceType, String streamUri, String xmlCapabilities) throws MessageException {
        switch (serviceType) {
            case LOCATION_DOCUMENT_SERVICE:
                return new LocationDocumentEntityInformation(streamUri, xmlCapabilities);
            case DOCUMENT_SERVICE:
                return new StreamEntityInformation(streamUri, xmlCapabilities);
        }
        throw throwServiceNotSupported(serviceType);
    }

    private static Entity getEmptyEntity(ServiceType serviceType) throws MessageException {
        switch (serviceType) {
            case LOCATION_DOCUMENT_SERVICE:
                return new LocationDocument();
            case DOCUMENT_SERVICE:
                return new Stream();
        }
        throw throwServiceNotSupported(serviceType);
    }

    private static MessageException throwServiceNotSupported(ServiceType serviceType) {
        return new MessageException("CiseMessageBuilder: The serviceType " + serviceType.value() + " is not supported");
    }

    /**
     * Pull Response
     **/

    public PullResponse buildPullResponseWithWmsCapabilities(PullRequest pullRequest, String xmlCapabilities, String streamUri) {
        var payloadEntity = getShomInformationEntity(serviceType, streamUri, xmlCapabilities);
        return buildPullResponse(pullRequest, payloadEntity.buildEntity());
    }

    public PullResponse buildPullResponseWithError(PullRequest pullRequest, String errorMsg) {
        return buildPullResponse(pullRequest, null, ResponseCodeType.END_POINT_NOT_FOUND, errorMsg);
    }

    public PullResponse buildPullResponseWithError(PullRequest pullRequest, WmsServerException wmsServerException) {
        int httpCode = wmsServerException.getHttpCode();
        String errorMessage = wmsServerException.getMessage();

        ResponseCodeType rType;
        switch (httpCode) {
            case 401:
                rType = ResponseCodeType.AUTHENTICATION_ERROR;
                errorMessage = "The WMS server credentials are wrong";
                break;
            case 404:
                rType = ResponseCodeType.END_POINT_NOT_FOUND;
                break;
            default:
                rType = ResponseCodeType.SERVER_ERROR;
                break;
        }
        return buildPullResponse(pullRequest, null, rType, errorMessage);
    }

    private PullResponse buildPullResponse(PullRequest pullRequest, Entity streamEntity) {
        return buildPullResponse(pullRequest, streamEntity, ResponseCodeType.SUCCESS, null);
    }

    private PullResponse buildPullResponse(PullRequest pullRequest, Entity payloadEntity, ResponseCodeType responseCodeType, String errrorMsg) {

        var reliabilityProfile = new ReliabilityProfile();
        reliabilityProfile.setRetryStrategy(RetryStrategyType.LOW_RELIABILITY);
        if (payloadEntity == null) {
            payloadEntity = getEmptyEntity(serviceType);
        }

        return PullResponseBuilder.newPullResponse()
                .creationDateTime(new Date())
                .correlationId(pullRequest.getCorrelationID())
                .contextId(pullRequest.getContextID())
                .generateId()
                .isRequiresAck(false)
                .priority(PriorityType.HIGH)
                .sender(buildService(serviceIdResponse))
                .recipient(pullRequest.getSender())
                .reliability(reliabilityProfile)
                .resultCode(responseCodeType)
                .errorDetail(StringUtils.isEmpty(errrorMsg) ? null : errrorMsg)
                .informationSecurityLevel(InformationSecurityLevelType.NON_SPECIFIED)
                .informationSensitivity(InformationSensitivityType.NON_SPECIFIED)
                .purpose(PurposeType.NON_SPECIFIED)
                .isPersonalData(false)
                .addEntity(payloadEntity)
                .build();
    }

    /**
     * Pull Request
     */

    public PullRequest buildPullRequest() {

        var reliabilityProfile = new ReliabilityProfile();
        reliabilityProfile.setRetryStrategy(RetryStrategyType.LOW_RELIABILITY);

        var pullRequest = PullRequestBuilder.newPullRequest()
                .creationDateTime(new Date())
                .contextId(UUID.randomUUID().toString())
                .generateId()
                .isRequiresAck(false)
                .priority(PriorityType.HIGH)
                .sender(buildService(serviceIdRequest))
                .recipient(buildService(serviceIdResponse))
                .reliability(reliabilityProfile)
                .pullType(PullType.REQUEST)
                .responseTimeout(360)
                .informationSecurityLevel(InformationSecurityLevelType.NON_SPECIFIED)
                .informationSensitivity(InformationSensitivityType.NON_SPECIFIED)
                .purpose(PurposeType.NON_SPECIFIED)
                .isPersonalData(false)
                .addEntity(getEmptyEntity(serviceType))
                .build();

        pullRequest.setCorrelationID(pullRequest.getMessageID());
        return pullRequest;
    }

    private Service buildService(String serviceId) {
        return ServiceBuilder.newService()
                .id(serviceId)
                .operation(ServiceOperationType.PULL)
                .type(serviceType)
                .build();
    }

    public String getServiceIdRequest() {
        return serviceIdRequest;
    }

    public String getServiceIdResponse() {
        return serviceIdResponse;
    }
}
