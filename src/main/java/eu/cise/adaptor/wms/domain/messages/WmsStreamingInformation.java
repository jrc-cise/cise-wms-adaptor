package eu.cise.adaptor.wms.domain.messages;

import eu.cise.datamodel.v1.entity.Entity;

import java.util.Optional;

/**
 * Carry the wms information for the map streaming. They consist in:
 * - Xml Capabilities retrieved from Wms Server
 * - Stream URL for the MapAgent
 * - builder for the CISE Entity that contains the information
 */
public interface WmsStreamingInformation {

    Optional<String> getXmlCapabilities();
    Optional<String> getStreamUri();
    Entity buildEntity();
}
