package eu.cise.adaptor.wms.domain.messages;

import eu.cise.datamodel.v1.entity.document.LocationDocument;
import eu.cise.datamodel.v1.entity.document.LocationDocumentType;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

/**
 * Wms Information contained in the LocationDocument Entity
 *
 * <LocationDocument>
 *      <Title>
 *     	    Is hard coded to 'WMS Services'
 *      </Title>
 *      <DocumentType>
 *     	    Is hard coded to MeteorologicalMaps (is a value coming from the datamodel)
 *      </DocumentType>
 *      <ReferenceURI>
 *  	 This is an example:
 * 		  file://eu.green/GetMap/eu.green.proxyserver.document.pull.prov.maps
 *      </ReferenceURI>
 *      <Metadata>
 *         <Description>
 * 		    	Is hard coded to 'WMS Capabilities'
 * 		    </Description>
 * 		    <FileSchema>
 * 			    The xml with the capabilities received from the Wms Server.
 * 		    </FileSchema>
 *     </Metadata>
 * </LocationDocument>
 */
public class LocationDocumentEntityInformation implements WmsStreamingInformation {

    private static final LocationDocumentType PAYLOAD_TYPE = LocationDocumentType.METEOROLOGICAL_MAPS;
    private static final String PAYLOAD_TITLE = "WMS Services";

    private final String streamUri;
    private final MetadataEntityInformation metadata;

    /**
     * Create the LocationDocument Entity with streamUri and xmlCapabilities
     *
     * @param streamUri
     * @param xmlCapabilities
     */
    public LocationDocumentEntityInformation(String streamUri, String xmlCapabilities) {
        this.streamUri = streamUri;
        this.metadata = new MetadataEntityInformation(xmlCapabilities);
    }

    /**
     * Retrieve the LocationDocument Entity from the PullResponse payload
     *
     * @param pullResponse
     */
    public LocationDocumentEntityInformation(PullResponse pullResponse) {

        Optional<LocationDocument> optionalLocationDocument = getLocation(pullResponse);
        if (optionalLocationDocument.isPresent()) {
            var locationDocument = optionalLocationDocument.get();
            this.streamUri = locationDocument.getReferenceURI();
            this.metadata = new MetadataEntityInformation(locationDocument.getMetadatas());

        } else {
            throw new MessageException("Location not found");
        }
    }

    @Override
    public Optional<String> getXmlCapabilities() {
        return this.metadata.getXmlCapabilities();
    }

    @Override
    public Optional<String> getStreamUri() {
        return StringUtils.isNotEmpty(streamUri) ? Optional.of(streamUri) : Optional.empty();
    }

    @Override
    public LocationDocument buildEntity() {

        LocationDocument locationDocument = new LocationDocument();

        locationDocument.setDocumentType(PAYLOAD_TYPE);
        locationDocument.setTitle(PAYLOAD_TITLE);
        locationDocument.setReferenceURI(streamUri);
        locationDocument.getMetadatas().add(metadata.buildMetadata());

        return locationDocument;
    }

    private Optional<LocationDocument> getLocation(PullResponse pullResponse) {
        return Optional.of(pullResponse.getPayload())
                .filter(XmlEntityPayload.class::isInstance)
                .map(i -> ((XmlEntityPayload) i).getAnies())
                .filter(CollectionUtils::isNotEmpty)
                .map(i -> i.get(0))
                .filter(LocationDocument.class::isInstance)
                .map(LocationDocument.class::cast);
    }
}
