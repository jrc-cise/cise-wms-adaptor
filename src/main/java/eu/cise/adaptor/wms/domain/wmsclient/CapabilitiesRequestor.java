package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.domain.messages.CiseMessageBuilder;
import eu.cise.adaptor.wms.resources.helpers.HttpErrors;
import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.servicemodel.v1.message.AcknowledgementType;
import eu.cise.servicemodel.v1.message.PullRequest;
import io.smallrye.mutiny.Uni;

import javax.ws.rs.core.Response;

public class CapabilitiesRequestor {

    private final Dispatcher dispatcher;
    private final String nodeAddress;
    private final CiseMessageBuilder ciseMessageBuilder;
    private final SyncAsyncCoordinator syncAsyncCoordinator;

    private final AdaptorLogger logger;

    public CapabilitiesRequestor(Dispatcher dispatcher, String nodeAddress, CiseMessageBuilder ciseMessageBuilder, SyncAsyncCoordinator syncAsyncCoordinator, AdaptorLogger logger) {

        this.dispatcher = dispatcher;
        this.nodeAddress = nodeAddress;
        this.ciseMessageBuilder = ciseMessageBuilder;
        this.syncAsyncCoordinator = syncAsyncCoordinator;
        this.logger = logger;
    }

    public Uni<Response> getCapabilities(String service, String request, String version) {

        // 1. Create the PullRequest
        PullRequest pullRequest = ciseMessageBuilder.buildPullRequest();
        logger.logWmsRequest(service, request, version, pullRequest.getMessageID());

        // 2. Send it to the node
        DispatchResult dispatchResult = dispatcher.send(pullRequest, nodeAddress);
        if (!dispatchResult.isOK() && dispatchResult.getHttpCode() == HttpErrors.SOCKET_ERROR) {
            // try again
            logger.logSocketError(pullRequest.getMessageID());
            dispatchResult = dispatcher.send(pullRequest, nodeAddress);
        }
        // 3. Check the synchronous answer
        return checkDispatchResult(dispatchResult);
    }

    private Uni<Response> buildAsyncResponse(String correlationId) {
        SyncAsyncCoordinator.ResponseLink link = syncAsyncCoordinator.createLink(correlationId);
        return Uni.createFrom().item(link).map(SyncAsyncCoordinator.ResponseLink::getResponse);
    }

    private Uni<Response> checkDispatchResult(DispatchResult dispatchResult) {

        logger.lodDispatchResult(dispatchResult);

        Uni<Response> response;
        if (dispatchResult.isOK()) {
            var ack = dispatchResult.getResult();
            if (ack.getAckCode() == AcknowledgementType.SUCCESS) {
                // 3. Put the http request on hold
                response = buildAsyncResponse(ack.getCorrelationID());
            } else {
                // CISE Request failure : return 404 to the http client
                response = Uni.createFrom().item(Response.status(404).build());
            }
        } else {
            // HTTP Request failure : return 404 to the http client
            response = Uni.createFrom().item(Response.status(404).build());
        }

        return response;
    }


}
