package eu.cise.adaptor.wms.domain.messages;

import eu.cise.datamodel.v1.entity.metadata.Metadata;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * CISE Metadata Entity that contains the xml Capabilities on the FileSchema tag.
 * Because its hugeness, it is compressed
 *
 * 	<Metadata>
 * 		<Description>
 * 			Is hard coded to 'WMS Capabilities'
 * 		</Description>
 * 		<FileSchema>
 * 			The xml with the capabilities received from the Wms Server.
 * 		</FileSchema>
 * 	</Metadata>
 */
public class MetadataEntityInformation {

    private static final String DESCRIPTION = "WMS Capabilities";

    private final String xmlCapabilities;

    /**
     * Create the Metadata Entity with xml capabilities
     *
     * @param xmlCapabilities
     */
    public MetadataEntityInformation(String xmlCapabilities) {
        this.xmlCapabilities = xmlCapabilities;
    }

    /**
     * Retrieve the Metadata Entity as the first Metadata Entity from the list received in the message payload
     *
     * @param metadatas
     * @throws MessageException
     */
    public MetadataEntityInformation(List<Metadata> metadatas) throws MessageException {

        if (CollectionUtils.isNotEmpty(metadatas)) {
            this.xmlCapabilities = getFileSchema(metadatas.get(0));
            if (StringUtils.isEmpty(this.xmlCapabilities)) {
                throw new MessageException("MetadataEntity: xmlCapabilities is empty");
            }
        } else {
            throw new MessageException("MetadataEntity: Metadata not found");
        }
    }

    private static byte[] compress(String xmlCapabilities) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GZIPOutputStream gzipOut = new GZIPOutputStream(baos);
            ObjectOutputStream objectOut = new ObjectOutputStream(gzipOut);
            objectOut.writeObject(xmlCapabilities);
            objectOut.close();
            byte[] bytes = baos.toByteArray();

            return bytes;
        } catch (IOException exception) {
            throw new MessageException("LocationEntity: compression exception " + exception.getMessage(), exception);
        }
    }

    private static String uncompress(byte[] bytesCapabilities) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(bytesCapabilities);
            GZIPInputStream gzipIn = new GZIPInputStream(bais);
            ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
            String xmlCapabilities = (String) objectIn.readObject();
            objectIn.close();

            return xmlCapabilities;
        } catch (IOException | ClassNotFoundException exception) {
            throw new MessageException("LocationEntity: uncompress exception " + exception.getMessage(), exception);
        }
    }

    public Optional<String> getXmlCapabilities() {
        return StringUtils.isNotEmpty(xmlCapabilities) ? Optional.of(xmlCapabilities) : Optional.empty();
    }

    public Metadata buildMetadata() {

        var metadata = new Metadata();
        metadata.setDescription(DESCRIPTION);

        byte[] compressedXml = compress(xmlCapabilities);
        var fileSchema = Base64.getEncoder().encodeToString(compressedXml);

        metadata.setFileSchema(fileSchema);

        return metadata;
    }

    private String getFileSchema(Metadata metadata) {

        var base64FileSchema = metadata.getFileSchema();
        if (StringUtils.isEmpty(base64FileSchema)) {
            throw new MessageException("MetadataEntity: The metadata file schema is empty");
        }
        byte[] compressedXml = Base64.getDecoder().decode(base64FileSchema);

        return uncompress(compressedXml);
    }
}
