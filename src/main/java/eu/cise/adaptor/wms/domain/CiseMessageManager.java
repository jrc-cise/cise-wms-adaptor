package eu.cise.adaptor.wms.domain;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.domain.wmsclient.CapabilitiesResponseHandler;
import eu.cise.adaptor.wms.domain.wmsserver.CapabilitiesRequestHandler;
import eu.cise.node.exceptions.NodeException;
import eu.cise.node.priority.PriorityAgent;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;

import java.util.function.Consumer;

public class CiseMessageManager implements PriorityAgent {

    private final CapabilitiesRequestHandler capabilitiesRequestHandler;
    private final CapabilitiesResponseHandler capabilitiesResponseHandler;
    private final AdaptorLogger logger;

    public CiseMessageManager(CapabilitiesRequestHandler capabilitiesRequestHandler, CapabilitiesResponseHandler capabilitiesResponseHandler, AdaptorLogger logger) {
        this.capabilitiesRequestHandler = capabilitiesRequestHandler;
        this.capabilitiesResponseHandler = capabilitiesResponseHandler;
        this.logger = logger;
    }

    @Override
    public void manageMessage(Message message) {

        if (message instanceof PullRequest) {
            // The PullRequest is asking the capabilities from the wms server
            manageMessageAsync(message, capabilitiesRequestHandler);
        } else if (message instanceof PullResponse) {
            // The PullResponse contains the wms capabilities
            manageMessageAsync(message, capabilitiesResponseHandler);
        } else {
            throw new NodeException("Received a " + message.getClass().getSimpleName() + " message. Only PullRequest and PullRequest are managed");
        }
    }

    private void manageMessageAsync(Message message, Consumer<Message> ciseStrategy) {

        Uni.createFrom()
                .item(message)
                .emitOn(Infrastructure.getDefaultWorkerPool())
                .onItem()
                .invoke(logger::logStartMessageManagement)
                .invoke(ciseStrategy)
                .subscribe().with(this::managementFinished);
    }

    private void managementFinished(Message message) {
        logger.logEndMessageManagement(message);
    }
}
