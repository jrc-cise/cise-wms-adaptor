package eu.cise.adaptor.wms.domain.wmsserver;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.domain.messages.CiseMessageBuilder;
import eu.cise.adaptor.wms.domain.messages.MessageException;
import eu.cise.adaptor.wms.resources.helpers.HttpErrors;
import eu.cise.adaptor.wms.wmsserver.WmsCapabilitiesServer;
import eu.cise.adaptor.wms.wmsserver.WmsServerException;
import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

public class CapabilitiesRequestHandler implements Consumer<Message> {

    private final String streamProducerServiceId;
    private final Dispatcher dispatcher;
    private final String nodeAddress;
    private final CiseMessageBuilder ciseMessageBuilder;
    private final WmsCapabilitiesServer wmsCapabilitiesServer;
    private final AdaptorLogger logger;

    public CapabilitiesRequestHandler(String streamProducerServiceId, Dispatcher dispatcher, String nodeAddress, CiseMessageBuilder ciseMessageBuilder, WmsCapabilitiesServer wmsCapabilitiesServer, AdaptorLogger logger) {
        this.streamProducerServiceId = streamProducerServiceId;
        this.dispatcher = dispatcher;
        this.nodeAddress = nodeAddress;
        this.ciseMessageBuilder = ciseMessageBuilder;
        this.wmsCapabilitiesServer = wmsCapabilitiesServer;
        this.logger = logger;
    }

    @Override
    public void accept(Message message) {
        PullRequest pullRequest = (PullRequest) message;
        PullResponse pullResponse;
        try {

            // 1. Retrieve the WMS Capabilities
            String xmlCapabilities = wmsCapabilitiesServer.getCapabilities();

            // 2. Build Stream URI
            String streamUri = buildStreamURI();

            // 3. Create the pull Response with the xml capabilities
            pullResponse = ciseMessageBuilder.buildPullResponseWithWmsCapabilities(pullRequest, xmlCapabilities, streamUri);

        } catch (WmsServerException httpError) {
            logger.logServerError(message.getMessageID(), httpError.getMessage());
            pullResponse = ciseMessageBuilder.buildPullResponseWithError(pullRequest, httpError);
        } catch (MessageException msgError) {
            logger.logServerError(message.getMessageID(), msgError.getMessage());
            pullResponse = ciseMessageBuilder.buildPullResponseWithError(pullRequest, msgError.getMessage());
        }

        // 4. Send to the Node
        DispatchResult dispatchResult = dispatcher.send(pullResponse, nodeAddress);
        if (!dispatchResult.isOK() && dispatchResult.getHttpCode() == HttpErrors.SOCKET_ERROR) {
            // try again
            logger.logSocketError(pullRequest.getMessageID());
            dispatchResult = dispatcher.send(pullRequest, nodeAddress);
        }

        logger.logCiseResponse(pullRequest.getMessageID(), pullRequest.getCorrelationID());
    }


    private String buildStreamURI() {
        String localHost;
        try {
            var localNoodeUrl = new URL(nodeAddress);
            localHost = localNoodeUrl.getHost();
        } catch (MalformedURLException e) {
            localHost = "localhost";
        }
        var schema = "file://";
        String mapAgentPath = "GetMap";
        String serviceId = streamProducerServiceId.trim();

        return schema +  localHost + "/" + mapAgentPath + "/" + serviceId;
    }
}
