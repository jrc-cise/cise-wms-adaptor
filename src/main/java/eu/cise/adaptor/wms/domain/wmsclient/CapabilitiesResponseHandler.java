package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.domain.messages.CiseMessageBuilder;
import eu.cise.adaptor.wms.domain.messages.MessageException;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.ResponseCodeType;
import eu.cise.servicemodel.v1.service.ServiceType;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;
import java.util.function.Consumer;

public class CapabilitiesResponseHandler implements Consumer<Message> {

    private final URL nodeEndpointUrl;
    private final SyncAsyncCoordinator syncAsyncCoordinator;
    private final AdaptorLogger logger;
    private final ServiceType serviceType;
    private final String adaptorWmsServiceUrl;

    public CapabilitiesResponseHandler(ServiceType serviceType,
                                       String nodeEndpointUrl,
                                       SyncAsyncCoordinator syncAsyncCoordinator,
                                       AdaptorLogger logger,
                                       String adaptorWmsServiceUrl) throws MalformedURLException {
        this.serviceType = serviceType;
        this.nodeEndpointUrl = new URL(nodeEndpointUrl);
        this.syncAsyncCoordinator = syncAsyncCoordinator;
        this.logger = logger;
        this.adaptorWmsServiceUrl = adaptorWmsServiceUrl;
    }

    @Override
    public void accept(Message message) {
        PullResponse pullResponse = (PullResponse) message;

        Optional<String> convertedXml = Optional.empty();
        if (pullResponse.getResultCode() == ResponseCodeType.SUCCESS) {

            try {
                var shomInfo = CiseMessageBuilder.getShomInformationEntity(serviceType, pullResponse);

                // 1. Get the capabilities xml
                Optional<String> xmlCapabilities = shomInfo.getXmlCapabilities();

                // 2. Get the StreamURI
                Optional<String> streamURI = shomInfo.getStreamUri();

                // 3. proxy conversion of the xml
                convertedXml = xmlCapabilities.map(xml -> capabilityProxyzation(xml, streamURI));

            } catch (MessageException messageException) {
                logger.logCiseResponseException(message.getMessageID(), messageException);
            }
        } else {
            logger.logCiseResponseError(message.getMessageID(), pullResponse.getResultCode().value(), pullResponse.getErrorDetail());
        }

        // 3. Give the xml to the correct http response on hold
        syncAsyncCoordinator.giveResponse(pullResponse.getCorrelationID(), pullResponse.getResultCode() , convertedXml);
    }

    public String capabilityProxyzation(String xmlCapabilities, Optional<String> streamServiceIdURI) {

        if (streamServiceIdURI.isEmpty()) {
            return xmlCapabilities;
        }

        try {
            URL wmsStreamUrl = new URL(nodeEndpointUrl, new URI(streamServiceIdURI.get()).getPath());

            CapabilitiesTransformer capabilitiesTransformer = new CapabilitiesTransformer(xmlCapabilities);

            capabilitiesTransformer.buildCapabilities(wmsStreamUrl, adaptorWmsServiceUrl);
            return capabilitiesTransformer.getXml();

        } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException | TransformerException | URISyntaxException err) {
            throw new MessageException("capabilityProxyzation error", err);
        }
    }
}
