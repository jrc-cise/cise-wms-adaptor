package eu.cise.adaptor.wms.security;


public interface AuthRepository {

    /**
     * This method is intended to return true when the Authorization is disabled
     *
     * @return true if no credential check is needed
     */
    boolean isPermitAll();

    /**
     * Verify if the couple user and password are registered in the system
     *
     * @param username username
     * @param password password
     * @return true if this couple is authorized
     */
    boolean isUserAndPasswordCorrect(String username, String password);

    /**
     * For future use when authorization is required
     *
     * Verify if the user has the privilege required for the roles
     *
     * @param username username
     * @param rolesSetAllowed set of allowed role
     *
     * @return true if the user can have access to at least one of the role presented in the rolesSetAllowed

    boolean isUserAllowed(final String username, final Set<String> rolesSetAllowed);
     */
}
