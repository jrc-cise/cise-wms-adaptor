package eu.cise.adaptor.wms.security;

import eu.cise.adaptor.wms.security.crypto.EncryptorAesGcmPassword;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class BasicAuthFileWmsCredentialRepository implements ServerWmsCredentialRepository {

    private final Pair<String, String> credentials;

    private final boolean isPermitAll;

    public BasicAuthFileWmsCredentialRepository(String filePath) {
        File authFile = new File(filePath);
        credentials = readBasicAuth(authFile);
        isPermitAll = StringUtils.isEmpty(credentials.getLeft());
    }

    @Override
    public boolean isPermitAll() {
        return isPermitAll;
    }

    /**
     * Retrieve the credentials stored in a file
     * If the file is not find or the credential are not in basic format, the authentication is ignored
     *
     * @return Pair with username (left value) and password (right value)
     */
    @Override
    public Pair<String, String> getWmsCredential() {
        return credentials;
    }

    private Pair<String, String> readBasicAuth(File authFile) {

        Pair<String, String> result = new ImmutablePair<>("", "");
        try (var scanner = new Scanner(new FileInputStream(authFile), StandardCharsets.UTF_8).useDelimiter("\n")) {
            if (scanner.hasNext()) {
                var line = EncryptorAesGcmPassword.decrypt(scanner.next());
                final StringTokenizer tokenizer = new StringTokenizer(line, ":");
                if (tokenizer.countTokens() == 2) {
                    String username = tokenizer.nextToken();
                    String password = tokenizer.nextToken();
                    result = new ImmutablePair<>(username, password);
                }
            }
        } catch (IllegalArgumentException | FileNotFoundException | InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | InvalidKeyException e) {
           // nothing to do
        }
        return result;
    }
}
