package eu.cise.adaptor.wms.security;

import eu.cise.adaptor.wms.security.crypto.EncryptorAesGcmPassword;
import org.apache.commons.collections.CollectionUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Implementation of the user and password repository using a file
 * The file contains user and password in basic auth format username:password
 * It is possible to have more than one user and password: for each couple, one line in the file
 */
public class BasicAuthFileRepository implements AuthRepository {

    // Contains user and password in Basic format user:password
    private final Set<String> authSet;

    private final boolean isPermitAll;

    /**
     * To create the instance, the file location is mandatory
     *
     * @param filePath location of the file with username and password
     */
    public BasicAuthFileRepository(String filePath)  {
        File authFile = new File(filePath);
        authSet = readBasicAuth(authFile);
        isPermitAll = CollectionUtils.isEmpty(authSet);
    }

    private Set<String> readBasicAuth(File authFile)  {

        Set<String> fileSet = new HashSet<>();
        try (var scanner = new Scanner(new FileInputStream(authFile), StandardCharsets.UTF_8).useDelimiter("\n")) {
            while (scanner.hasNext()) {
                var line = scanner.next();
                fileSet.add(EncryptorAesGcmPassword.decrypt(line));
            }
        } catch (IllegalArgumentException | FileNotFoundException | InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | InvalidKeyException e) {
            // nothing to do
        }
        return fileSet;
    }

    @Override
    public boolean isPermitAll() {
        return isPermitAll;
    }

    @Override
    public boolean isUserAndPasswordCorrect(String username, String password) {
        return isPermitAll || authSet.contains(username + ":" + password);
    }


    /* For future use when authorization is required
    @Override
    public boolean isUserAllowed(final String username, final Set<String> rolesSetAllowed)    {
        boolean isAllowed = false;

        //Step 1. Fetch password from database and match with password in argument
        //If both match then get the defined role for user from database and continue; else return isAllowed [false]
        //Access the database and do this part yourself
        //String userRole = userMgr.getUserRole(username);
        String userRole = "ADMIN";

        //Step 2. Verify user role
        if (rolesSetAllowed.contains(userRole))
        {
            isAllowed = true;
        }
        return isAllowed;
    }
    */
}
