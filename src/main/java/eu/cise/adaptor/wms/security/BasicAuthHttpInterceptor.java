package eu.cise.adaptor.wms.security;

import eu.cise.adaptor.wms.AdaptorLogger;
import io.vertx.core.http.HttpServerRequest;
import org.apache.commons.collections.CollectionUtils;
import java.util.Base64;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This interceptor verifies the access permissions for a user
 * based on username and password provided in request
 */
@Provider
public class BasicAuthHttpInterceptor implements javax.ws.rs.container.ContainerRequestFilter {
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final ServerResponse ACCESS_DENIED = new ServerResponse("Access denied for this resource", 401, new Headers<>());
    private static final ServerResponse ACCESS_FORBIDDEN = new ServerResponse("Nobody can access this resource", 403, new Headers<>());
    private static final ServerResponse SERVER_ERROR = new ServerResponse("INTERNAL SERVER ERROR", 500, new Headers<>());


    @Context
    UriInfo info;

    @Context
    HttpServerRequest request;

    @Inject
    AuthRepository authRepository;

    @Inject
    AdaptorLogger logger;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();

        if (method.isAnnotationPresent(PermitAll.class) || authRepository.isPermitAll()) {
            return;
        }

        if (method.isAnnotationPresent(DenyAll.class)) {
            requestContext.abortWith(ACCESS_FORBIDDEN);
            logger.logSecureNotAccepted(ACCESS_FORBIDDEN.getReasonPhrase(), request.remoteAddress().hostAddress(), request.path());
            return;
        }

        // Get request headers
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();

        // Fetch authorization header
        final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

        // If no authorization information present; block access
        if (CollectionUtils.isEmpty(authorization)) {
            requestContext.abortWith(ACCESS_DENIED);
            logger.logSecureNotAccepted("Authorization property not found", request.remoteAddress().hostAddress(), request.path());
            return;
        }

        // Get encoded username and password
        final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

        // Decode username and password
        String usernameAndPassword;
        try {
            usernameAndPassword = new String(Base64.getDecoder().decode(encodedUserPassword));
        } catch (IllegalArgumentException ex) {
            requestContext.abortWith(ACCESS_DENIED);
            logger.logSecureNotAccepted("Wrong basic auth format", request.remoteAddress().hostAddress(), request.path());
            return;
        }
        // Split username and password tokens
        final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        if (tokenizer.countTokens() != 2) {
            requestContext.abortWith(ACCESS_DENIED);
            logger.logSecureNotAccepted("Wrong basic auth format", request.remoteAddress().hostAddress(), request.path());
            return;
        }

        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();

        // Verifying Username and password
        if (!authRepository.isUserAndPasswordCorrect(username, password)) {
            requestContext.abortWith(ACCESS_DENIED);
            logger.logSecureNotAccepted(username, request.remoteAddress().hostAddress(), request.path());
            return;
        }

        /* For future use
        // Verify user access
        if (method.isAnnotationPresent(RolesAllowed.class)) {
            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

            // Is user valid ?
            if (!authRepository.isUserAllowed(username, rolesSet)) {
                requestContext.abortWith(ACCESS_DENIED);
                logger.logSecureNotAccepted(username, request.remoteAddress().hostAddress(), request.path());
            }
        }
        */

        logger.logSecureAccepted(username, request.remoteAddress().hostAddress(), request.path());
    }
}