package eu.cise.adaptor.wms.security;

import org.apache.commons.lang3.tuple.Pair;

public interface ServerWmsCredentialRepository {

    /**
     * This method is intended to return true when the Authorization is disabled
     * @return true if no credential check is needed
     */
    boolean isPermitAll();

    Pair<String, String> getWmsCredential();
}
