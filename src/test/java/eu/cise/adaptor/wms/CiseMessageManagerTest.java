package eu.cise.adaptor.wms;

import eu.cise.adaptor.wms.domain.CiseMessageManager;
import eu.cise.adaptor.wms.domain.wmsclient.CapabilitiesResponseHandler;
import eu.cise.adaptor.wms.domain.wmsserver.CapabilitiesRequestHandler;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.eucise.helpers.PullRequestBuilder;
import eu.eucise.helpers.PullResponseBuilder;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class CiseMessageManagerTest {

    AdaptorLogger logger = mock(AdaptorLogger.class);

    @Test
    void it_manage_pullrequest() {
        CapabilitiesRequestHandler capabilitiesRequestHandler = mock(CapabilitiesRequestHandler.class);
        CapabilitiesResponseHandler capabilitiesResponseHandler = mock(CapabilitiesResponseHandler.class);

        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("request-context-id")
                .correlationId("request-correlation-id")
                .generateId()
                .build();

        CiseMessageManager ciseMessageManager = new CiseMessageManager(capabilitiesRequestHandler, capabilitiesResponseHandler, logger);

        ciseMessageManager.manageMessage(pullRequest);
        verify(capabilitiesRequestHandler, timeout(5000)).accept(pullRequest);
    }

    @Test
    void it_manage_pullresponse() {
        CapabilitiesRequestHandler capabilitiesRequestHandler = mock(CapabilitiesRequestHandler.class);
        CapabilitiesResponseHandler capabilitiesResponseHandler = mock(CapabilitiesResponseHandler.class);

        PullResponse pullResponse = PullResponseBuilder.newPullResponse()
                .contextId("response-context-id")
                .correlationId("response-correlation-id")
                .generateId()
                .build();

        CiseMessageManager ciseMessageManager = new CiseMessageManager(capabilitiesRequestHandler, capabilitiesResponseHandler, logger);

        ciseMessageManager.manageMessage(pullResponse);
        verify(capabilitiesResponseHandler, timeout(5000)).accept((pullResponse));

    }

}