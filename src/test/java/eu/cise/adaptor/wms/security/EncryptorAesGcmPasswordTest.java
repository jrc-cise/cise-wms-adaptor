package eu.cise.adaptor.wms.security;

import eu.cise.adaptor.wms.security.crypto.EncryptorAesGcmPassword;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class EncryptorAesGcmPasswordTest {

    private final String cred1 = "user1:password1";
    private final String cred2 = "user2:password2";
    private final String cred3 = "user3:password3";

    private final String CRIPTED_FILE_NAME = "./target/test-classes/targetcrepted.txt";

    @Test
    @Order(1)
    void it_encrypt() throws Exception {

        var enc1 = EncryptorAesGcmPassword.encrypt(cred1);
        var enc2 = EncryptorAesGcmPassword.encrypt(cred2);
        var enc3 = EncryptorAesGcmPassword.encrypt(cred3);

        assertNotNull(enc1);
        assertNotNull(enc2);
        assertNotNull(enc3);

        BufferedWriter writer = new BufferedWriter(new FileWriter(CRIPTED_FILE_NAME));
        writer.write(enc1);
        writer.newLine();
        writer.write(enc2);
        writer.newLine();
        writer.write(enc3);
        writer.newLine();
        writer.close();
    }

    @Test
    @Order(2)
    void it_decrypt() {
        String[] credentials = new String[]{cred1, cred2, cred3};
        int idx = 0;
        try (var scanner = new Scanner(new FileInputStream(CRIPTED_FILE_NAME), StandardCharsets.UTF_8).useDelimiter("\n")) {
            while (scanner.hasNext()) {
                var line = scanner.next();
                var decoded = EncryptorAesGcmPassword.decrypt(line);
                assertEquals(credentials[idx++], decoded);
            }
        } catch (FileNotFoundException ignored) {
            // nothing to do
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}