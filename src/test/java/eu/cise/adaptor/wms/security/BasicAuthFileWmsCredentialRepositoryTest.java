package eu.cise.adaptor.wms.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasicAuthFileWmsCredentialRepositoryTest {

    @Test
    void it_gets_wms_credentials() {
        var myClass = new BasicAuthFileWmsCredentialRepository("./target/test-classes/wmscredentials.txt");
        var credentials = myClass.getWmsCredential();
        assertFalse(myClass.isPermitAll());
        assertEquals("wmsuser", credentials.getLeft());
        assertEquals("wmspassword", credentials.getRight());
    }

    @Test
    void it_checks_permits_all() {
        var myClass = new BasicAuthFileWmsCredentialRepository("./target/test-classes/filenotfound.txt");
        assertTrue(myClass.isPermitAll());
    }
}