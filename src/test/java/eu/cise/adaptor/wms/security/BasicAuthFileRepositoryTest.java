package eu.cise.adaptor.wms.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
class BasicAuthFileRepositoryTest {

    @Test
    void it_found_the_right_user_and_password()  {

        var myClass = new BasicAuthFileRepository("./target/test-classes/authfile.txt");
        assertFalse(myClass.isPermitAll());
        assertTrue(myClass.isUserAndPasswordCorrect("user1", "password1"));
        assertTrue(myClass.isUserAndPasswordCorrect("user2", "password2"));
        assertTrue(myClass.isUserAndPasswordCorrect("user3", "password3"));
    }

    @Test
    void it_found_the_wrong_user_and_password()  {

        var myClass = new BasicAuthFileRepository("./target/test-classes/authfile.txt");
        assertFalse(myClass.isPermitAll());
        assertFalse(myClass.isUserAndPasswordCorrect("user4", "password1"));
        assertFalse(myClass.isUserAndPasswordCorrect("user5", "password2"));
        assertFalse(myClass.isUserAndPasswordCorrect("user6", "password3"));
    }

    @Test
    void it_checks_permits_all()  {

        var myClass = new BasicAuthFileRepository("./target/test-classes/wrongfilename.txt");
        assertTrue(myClass.isPermitAll());
        assertTrue(myClass.isUserAndPasswordCorrect("user4", "password1"));
        assertTrue(myClass.isUserAndPasswordCorrect("user5", "password2"));
        assertTrue(myClass.isUserAndPasswordCorrect("user6", "password3"));
    }
}