package eu.cise.adaptor.wms.domain.messages;

import eu.cise.datamodel.v1.entity.metadata.Metadata;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MetadataEntityInformationTest {

    @Test
    void it_check_sent_and_recv_capabilities() throws IOException {

        String xmlCapabilitiesSent = getXmlCapabilities();

        var metadataEntitySent = new MetadataEntityInformation(xmlCapabilitiesSent);
        var metadataSent = metadataEntitySent.buildMetadata();

        var metadataRecv = new MetadataEntityInformation(List.of(metadataSent));
        var xmlCapabilitiesRecv = metadataRecv.getXmlCapabilities();

        assertTrue(xmlCapabilitiesRecv.isPresent());
        assertEquals(xmlCapabilitiesSent, xmlCapabilitiesRecv.get());
    }


    @Test
    void it_give_exception_when_no_metadata_or_capability() {

        assertThrows(MessageException.class, () -> new MetadataEntityInformation(new LinkedList<>()));
        assertThrows(MessageException.class, () -> new MetadataEntityInformation(List.of(new Metadata())));
    }

    private String getXmlCapabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlShomCapabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }

}