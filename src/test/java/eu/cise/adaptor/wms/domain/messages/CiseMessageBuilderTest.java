package eu.cise.adaptor.wms.domain.messages;

import eu.cise.adaptor.wms.helpers.CiseConfigLocationHelper;
import eu.cise.adaptor.wms.helpers.CiseConfigStreamHelper;
import eu.cise.datamodel.v1.entity.document.LocationDocument;
import eu.cise.datamodel.v1.entity.document.LocationDocumentType;
import eu.cise.datamodel.v1.entity.document.Stream;
import eu.cise.datamodel.v1.entity.document.StreamType;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.eucise.helpers.PullRequestBuilder;
import eu.eucise.xml.DefaultXmlMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CiseMessageBuilderTest {


    static CiseMessageBuilder ciseMessageStreamFactory;
    static CiseMessageBuilder ciseMessageLocationFactory;

    @BeforeAll
    static void init() {
        ciseMessageStreamFactory = new CiseMessageBuilder(new CiseConfigStreamHelper());
        ciseMessageLocationFactory = new CiseMessageBuilder(new CiseConfigLocationHelper());
    }

    @Test
    void it_builds_pull_response_positive_answer_stream_entity() throws IOException {
        String xmlCapabilities = "<hello>this is a test</hello>";
        String streamUri = "proxy://nodeB/eu.nodeB.participantB.document.pull.prov.maps?access_token=XXXXXXXXXXXXX&token_type=bearer&expires_in=3600";

        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        PullResponse pullResponse = ciseMessageStreamFactory.buildPullResponseWithWmsCapabilities(pullRequest, xmlCapabilities, streamUri);
        assertNotNull(pullResponse);

        /*
        try {
            String result = new DefaultXmlMapper.Pretty().toXML(pullResponse);
            System.out.println(result);
        } catch (RuntimeException ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }
        */

        assertEquals("context-id", pullResponse.getContextID());
        assertEquals("correlation-id", pullResponse.getCorrelationID());
        assertNotEquals(pullRequest.getMessageID(), pullResponse.getMessageID());
        assertNull(pullResponse.getErrorDetail());

        try {
            // Check the payload
            Stream entity = (Stream) ((XmlEntityPayload) pullResponse.getPayload()).getAnies().get(0);
            assertNotNull(entity);

            var metadata = entity.getMetadatas().get(0);
            assertEquals("WMS Capabilities", metadata.getDescription());
          //  assertEquals(xmlCapabilities, StreamEntity.getXmlCapabilities(metadata));

            assertEquals(StreamType.IMAGE_MAP, entity.getStreamType());
            assertEquals(streamUri, entity.getStreamURI());


        } catch (RuntimeException  ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }
    }

    @Test
    void it_builds_pull_response_positive_answer_location_entity() throws IOException {
        String xmlCapabilities = "<hello>this is a test</hello>";
        String streamUri = "proxy://nodeB/eu.nodeB.participantB.document.pull.prov.maps?access_token=XXXXXXXXXXXXX&token_type=bearer&expires_in=3600";

        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        PullResponse pullResponse = ciseMessageLocationFactory.buildPullResponseWithWmsCapabilities(pullRequest, xmlCapabilities, streamUri);
        assertNotNull(pullResponse);

        /*
        try {
            String result = new DefaultXmlMapper.Pretty().toXML(pullResponse);
            System.out.println(result);
        } catch (RuntimeException ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }
        */

        assertEquals("context-id", pullResponse.getContextID());
        assertEquals("correlation-id", pullResponse.getCorrelationID());
        assertNotEquals(pullRequest.getMessageID(), pullResponse.getMessageID());
        assertNull(pullResponse.getErrorDetail());

        try {
            // Check the payload
            LocationDocument entity = (LocationDocument) ((XmlEntityPayload) pullResponse.getPayload()).getAnies().get(0);
            assertNotNull(entity);

            var metadata = entity.getMetadatas().get(0);
            assertEquals("WMS Capabilities", metadata.getDescription());
            //     assertEquals(xmlCapabilities, StreamEntity.getXmlCapabilities(metadata));

            assertEquals(LocationDocumentType.METEOROLOGICAL_MAPS, entity.getDocumentType());
            assertEquals(streamUri, entity.getReferenceURI());


        } catch (RuntimeException  ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }
    }

    @Test
    void it_builds_pull_response_negative_answer() {

        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        String errorMsg = "This is a test";
        PullResponse pullResponse = ciseMessageStreamFactory.buildPullResponseWithError(pullRequest, errorMsg);
        assertNotNull(pullResponse);

        try {
            String result = new DefaultXmlMapper.Pretty().toXML(pullResponse);
            System.out.println(result);
        } catch (RuntimeException ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }

        assertEquals("context-id", pullResponse.getContextID());
        assertEquals("correlation-id", pullResponse.getCorrelationID());
        assertNotEquals(pullRequest.getMessageID(), pullResponse.getMessageID());
        assertEquals(errorMsg, pullResponse.getErrorDetail());
    }

    @Test
    void it_build_pull_request() {
        PullRequest pullRequest = ciseMessageStreamFactory.buildPullRequest();

        try {
            String result = new DefaultXmlMapper.Pretty().toXML(pullRequest);
            System.out.println(result);
        } catch (RuntimeException ex) {
            fail("Message is not compliant: " + ex.getMessage());
        }
    }
}