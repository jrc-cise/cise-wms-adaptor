package eu.cise.adaptor.wms.domain.messages;

import eu.cise.servicemodel.v1.message.InformationSecurityLevelType;
import eu.cise.servicemodel.v1.message.InformationSensitivityType;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.PurposeType;
import eu.eucise.helpers.PullResponseBuilder;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class StreamEntityTest {

    @Test
    void it_build_stream_entity() throws IOException {

        String streamUri = "http://nodeB/service.nodeB?access_token=token&token_type=bearer&expires_in=3600";
        String xmlCapabilities = getXmlCapabilities();

        StreamEntityInformation streamEntitySent = new StreamEntityInformation(streamUri, xmlCapabilities);

        var pullResponse = PullResponseBuilder.newPullResponse().addEntity(streamEntitySent.buildEntity())
                .informationSecurityLevel(InformationSecurityLevelType.NON_SPECIFIED)
                .informationSensitivity(InformationSensitivityType.NON_SPECIFIED)
                .purpose(PurposeType.NON_SPECIFIED)
                .isPersonalData(false)
                .build();

        StreamEntityInformation streamEntityReceived = new StreamEntityInformation(pullResponse);

        assertEquals(streamEntitySent.getStreamUri(), streamEntityReceived.getStreamUri());
        assertEquals(streamEntitySent.getXmlCapabilities(), streamEntityReceived.getXmlCapabilities());
    }

    @Test
    void it_get_stream_form_pull_response() throws IOException {
        PullResponse pullResponse = getPullResponse();

        StreamEntityInformation streamEntityReceived = new StreamEntityInformation(pullResponse);

        assertTrue(streamEntityReceived.getStreamUri().isPresent());
        assertTrue(streamEntityReceived.getXmlCapabilities().isPresent());


    }

    private String getXmlCapabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlShomCapabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }

    private PullResponse getPullResponse() throws IOException {
        XmlMapper xmlMapper = new DefaultXmlMapper();
        var resourcePath = new File("./target/test-classes/PullResponse_stream_test.xml");
        var xml = new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
        return xmlMapper.fromXML(xml);
    }

}