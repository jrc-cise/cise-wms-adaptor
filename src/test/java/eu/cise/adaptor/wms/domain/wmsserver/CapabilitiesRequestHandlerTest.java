package eu.cise.adaptor.wms.domain.wmsserver;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.config.CiseConfig;
import eu.cise.adaptor.wms.domain.messages.CiseMessageBuilder;
import eu.cise.adaptor.wms.helpers.CiseConfigLocationHelper;
import eu.cise.adaptor.wms.helpers.ProxyConfigHelper;
import eu.cise.adaptor.wms.security.BasicAuthFileWmsCredentialRepository;
import eu.cise.adaptor.wms.security.ServerWmsCredentialRepository;
import eu.cise.adaptor.wms.wmsserver.WmsCapabilitiesServer;
import eu.cise.adaptor.wms.wmsserver.WmsClient;
import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.ResponseCodeType;
import eu.eucise.helpers.PullRequestBuilder;
import eu.eucise.helpers.PullResponseBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import javax.ws.rs.WebApplicationException;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


class CapabilitiesRequestHandlerTest {

    private final ProxyConfigHelper proxyConfig = new ProxyConfigHelper();
    private final CiseConfig ciseConfig = new CiseConfigLocationHelper();
    private final ServerWmsCredentialRepository wmsRepository = new BasicAuthFileWmsCredentialRepository("./target/test-classes/wmscredential.txt");
    Dispatcher dispatcher;
    WmsCapabilitiesServer wmsCapabilitiesServer;
    CiseMessageBuilder ciseMessageBuilder;

    CapabilitiesRequestHandlerTest() throws FileNotFoundException {
    }

    @BeforeEach
    public void setup() {
        dispatcher = mock(Dispatcher.class);
    }

    @Test
    void it_test_accept_happy_path() throws IOException {
        String nodeAddress = "http://cise-node";
        String xmlCapabilities = "<capabilities/>";

        wmsCapabilitiesServer = mock(WmsCapabilitiesServer.class);
        when(wmsCapabilitiesServer.getCapabilities()).thenReturn(xmlCapabilities);

        PullResponse pullResponse = PullResponseBuilder.newPullResponse().build();
        ciseMessageBuilder = mock(CiseMessageBuilder.class);
        when(ciseMessageBuilder.buildPullResponseWithWmsCapabilities(any(), any(), any())).thenReturn(pullResponse);
        when(ciseMessageBuilder.getServiceIdResponse()).thenReturn("test-id");
        DispatchResult mokedResult = new DispatchResult(true, null);
        when(dispatcher.send(any(), any())).thenReturn(mokedResult);

        AdaptorLogger logger = mock(AdaptorLogger.class);
        CapabilitiesRequestHandler capabilitiesRequestHandler = new CapabilitiesRequestHandler(ciseConfig.serviceIdResponse(), dispatcher, nodeAddress, ciseMessageBuilder, wmsCapabilitiesServer, logger);
        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        capabilitiesRequestHandler.accept(pullRequest);

        verify(dispatcher).send(pullResponse, nodeAddress);
        verify(wmsCapabilitiesServer).getCapabilities();
        verify(ciseMessageBuilder).buildPullResponseWithWmsCapabilities(eq(pullRequest), eq(xmlCapabilities), any());
    }

    @Test
    void it_handle_wms_server_exception()  {

        String nodeAddress = "http://cise-node";

        // Mock logger
        AdaptorLogger logger = mock(AdaptorLogger.class);

        // Mock answer form wmsClient with error
        String errMessage = "this is a test";
        WebApplicationException excep = new WebApplicationException(errMessage);
        WmsClient wmsClient = Mockito.mock(WmsClient.class);
        doThrow(excep).when(wmsClient).getCapabilities(any());

        wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient, wmsRepository);

        ciseMessageBuilder = new CiseMessageBuilder(ciseConfig);

        // Mock Dispatcher
        DispatchResult mokedResult = new DispatchResult(true, null);
        when(dispatcher.send(any(), any())).thenReturn(mokedResult);

        CapabilitiesRequestHandler capabilitiesRequestHandler = new CapabilitiesRequestHandler(ciseConfig.serviceIdResponse(), dispatcher, nodeAddress, ciseMessageBuilder, wmsCapabilitiesServer, logger);
        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        capabilitiesRequestHandler.accept(pullRequest);
        ArgumentCaptor<Message> pullResponseCaptor =  ArgumentCaptor.forClass(Message.class);
        verify(dispatcher).send(pullResponseCaptor.capture(), eq(nodeAddress));

        PullResponse response = (PullResponse)pullResponseCaptor.getValue();
        assertEquals(ResponseCodeType.SERVER_ERROR,response.getResultCode());
        assertEquals(errMessage, response.getErrorDetail());
    }

    @Test
    void it_handle_no_auth_error_401() {

        String nodeAddress = "http://cise-node";

        // Mock logger
        AdaptorLogger logger = mock(AdaptorLogger.class);

        // Mock answer form wmsClient with error
        WebApplicationException excep = new WebApplicationException(401);
        WmsClient wmsClient = Mockito.mock(WmsClient.class);
        doThrow(excep).when(wmsClient).getCapabilities(any());

        wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient, wmsRepository);

        ciseMessageBuilder = new CiseMessageBuilder(ciseConfig);

        // Mock Dispatcher
        DispatchResult mokedResult = new DispatchResult(true, null);
        when(dispatcher.send(any(), any())).thenReturn(mokedResult);

        CapabilitiesRequestHandler capabilitiesRequestHandler = new CapabilitiesRequestHandler(ciseConfig.serviceIdResponse(), dispatcher, nodeAddress, ciseMessageBuilder, wmsCapabilitiesServer, logger);
        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        capabilitiesRequestHandler.accept(pullRequest);
        ArgumentCaptor<Message> pullResponseCaptor =  ArgumentCaptor.forClass(Message.class);
        verify(dispatcher).send(pullResponseCaptor.capture(), eq(nodeAddress));

        PullResponse response = (PullResponse)pullResponseCaptor.getValue();
        assertEquals(ResponseCodeType.AUTHENTICATION_ERROR,response.getResultCode());
        assertEquals("The WMS server credentials are wrong", response.getErrorDetail());
    }

    @Test
    void it_handle_runtime_exception() {

        String nodeAddress = "http://cise-node";

        // Mock logger
        AdaptorLogger logger = mock(AdaptorLogger.class);

        // Mock answer form wmsClient with error
        String errMessage = "this is runtime exception test";
        RuntimeException excep = new RuntimeException(errMessage);
        WmsClient wmsClient = Mockito.mock(WmsClient.class);
        doThrow(excep).when(wmsClient).getCapabilities(any());

        wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient , wmsRepository);

        ciseMessageBuilder = new CiseMessageBuilder(ciseConfig);

        // Mock Dispatcher
        DispatchResult mokedResult = new DispatchResult(true, null);
        when(dispatcher.send(any(), any())).thenReturn(mokedResult);

        CapabilitiesRequestHandler capabilitiesRequestHandler = new CapabilitiesRequestHandler(ciseConfig.serviceIdResponse(), dispatcher, nodeAddress, ciseMessageBuilder, wmsCapabilitiesServer, logger);
        PullRequest pullRequest = PullRequestBuilder.newPullRequest()
                .contextId("context-id")
                .correlationId("correlation-id")
                .generateId()
                .build();

        capabilitiesRequestHandler.accept(pullRequest);
        ArgumentCaptor<Message> pullResponseCaptor =  ArgumentCaptor.forClass(Message.class);
        verify(dispatcher).send(pullResponseCaptor.capture(), eq(nodeAddress));

        PullResponse response = (PullResponse)pullResponseCaptor.getValue();
        assertEquals(ResponseCodeType.SERVER_ERROR,response.getResultCode());
        assertEquals(excep.getClass().getName() + ": this is runtime exception test", response.getErrorDetail());
    }
}