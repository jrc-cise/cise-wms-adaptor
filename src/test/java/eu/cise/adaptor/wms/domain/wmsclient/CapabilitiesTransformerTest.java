package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.helpers.TestHelpers;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CapabilitiesTransformerTest {

    @Test
    void it_convert_arcgis_capabilities() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        var myTransformer = new CapabilitiesTransformer(getXmlArcgisCapabilities());

        String adaptorWmsServiceUrl = "http://wms-adaptor.org:9092";
        String wmsStreamUrl = "http://proxy.net:8080/GetMap/service.provider.map";
        myTransformer.buildCapabilities(new URL(wmsStreamUrl), adaptorWmsServiceUrl);

        String xmlTransformed = myTransformer.getXml();
        //System.out.println(xmlTransformed);

        // Check the number of adaptorWmsServiceUrl : 2 (Get and Post)
        String[] splittedString = xmlTransformed.split(adaptorWmsServiceUrl);
        assertEquals(3, splittedString.length);

        // Check the number of wmsStreamUrl : 6
        splittedString = xmlTransformed.split(wmsStreamUrl);
        assertEquals(7, splittedString.length);

        // Check the original wms url : 2 (schemaLocation and OnlineResource)
        splittedString = xmlTransformed.split("http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer");
        assertEquals(3, splittedString.length);
    }

    @Test
    void it_convert_ncwms2_capabilities() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        var myTransformer = new CapabilitiesTransformer(getXmlNcwms2Capabilities());

        String adaptorWmsServiceUrl = "http://wms-adaptor.org:9092";
        String wmsStreamUrl = "http://proxy.net:8080/GetMap/service.provider.map";
        myTransformer.buildCapabilities(new URL(wmsStreamUrl), adaptorWmsServiceUrl);

        String xmlTransformed = myTransformer.getXml();
       // System.out.println(xmlTransformed);

        // Check the number of adaptorWmsServiceUrl : 2 (Get and Post)
        String[] splittedString = xmlTransformed.split(adaptorWmsServiceUrl);
        assertEquals(3, splittedString.length);

        // Check the number of wmsStreamUrl : 4 (GetMap and GetFeatures tags)
        splittedString = xmlTransformed.split(wmsStreamUrl);
        assertEquals(5, splittedString.length);

        // Check the original wms url : 1 (OnlineResource)
        splittedString = xmlTransformed.split("https://services.data.shom.fr/ncwms2/wms");
        assertEquals(2, splittedString.length);
    }


    @Test
    void it_convert_inspire_capabilities() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        var myTransformer = new CapabilitiesTransformer(getXmlInspireCapabilities());

        String adaptorWmsServiceUrl = "http://wms-adaptor.org:9092";
        String wmsStreamUrl = "http://proxy.net:8080/GetMap/service.provider.map";
        myTransformer.buildCapabilities(new URL(wmsStreamUrl), adaptorWmsServiceUrl);

        String xmlTransformed = myTransformer.getXml();
     //   System.out.println(xmlTransformed);

        // Check the number of adaptorWmsServiceUrl : 1 (Get or Post)
        int adaptorWMSServiceURLOccurs = StringUtils.countMatches(xmlTransformed, adaptorWmsServiceUrl);
        assertEquals(2, adaptorWMSServiceURLOccurs);

        // Check the original wms url : 1 (OnlineResource)
        int origServiceURLOccur = StringUtils.countMatches(xmlTransformed, "http://wpshm202s:8001");
        assertEquals(1, origServiceURLOccur);

        // CSW layer checks. All layers must be maintained and chaned to wmsStreamURL
        // Check the number of wmsStreamUrl : 4 (GetMap and GetFeatures tags and all layers
        int wmsStreamURL = StringUtils.countMatches(xmlTransformed, wmsStreamUrl);
        int onlineResourceOccurs = StringUtils.countMatches(xmlTransformed, "OnlineResource");
        assertEquals(273, onlineResourceOccurs);

        assertEquals(273-adaptorWMSServiceURLOccurs-origServiceURLOccur, wmsStreamURL);

    }
    // 8

    @Test
    void it_convert_mixed_capabilities() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        var myTransformer = new CapabilitiesTransformer(getXmlMixedCapabilities());

        String adaptorWmsServiceUrl = "http://wms-adaptor.org:9092";
        String wmsStreamUrl = "http://proxy.net:8080/GetMap/service.provider.map";
        myTransformer.buildCapabilities(new URL(wmsStreamUrl), adaptorWmsServiceUrl);

        String xmlTransformed = myTransformer.getXml();
        //   System.out.println(xmlTransformed);

        // Check the number of adaptorWmsServiceUrl : 1 (Get or Post)
        int adaptorWMSServiceURLOccurs = StringUtils.countMatches(xmlTransformed, adaptorWmsServiceUrl);
        assertEquals(2, adaptorWMSServiceURLOccurs);

        // Check the original wms url : 1 (OnlineResource)
        int origServiceURLOccur = StringUtils.countMatches(xmlTransformed, "http://wpshm202s:8001");
        assertEquals(1, origServiceURLOccur);

        // CSW layer checks. All layers must be maintained and chaned to wmsStreamURL
        // Check the number of wmsStreamUrl : 4 (GetMap and GetFeatures tags and all layers
        int wmsStreamURL = StringUtils.countMatches(xmlTransformed, wmsStreamUrl);
        int onlineResourceOccurs = StringUtils.countMatches(xmlTransformed, "OnlineResource");
        assertEquals(273, onlineResourceOccurs);

        assertEquals(273-adaptorWMSServiceURLOccurs-origServiceURLOccur, wmsStreamURL);

    }

    private String getXmlNcwms2Capabilities() throws IOException {
        return TestHelpers.readResource("XmlNcwms2Capabilities.xml");
    }

    private String getXmlInspireCapabilities() throws IOException {
        return TestHelpers.readResource("XmlInspireCapabilities.xml");
    }

    private String getXmlArcgisCapabilities() throws IOException {
        return TestHelpers.readResource("XmlArcgisCapabilities.xml");
    }

    private String getXmlMixedCapabilities() throws IOException {
        return TestHelpers.readResource("XmlMixedCapabilities.xml");
    }


}