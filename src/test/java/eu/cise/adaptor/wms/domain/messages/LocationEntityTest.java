package eu.cise.adaptor.wms.domain.messages;

import eu.cise.servicemodel.v1.message.*;
import eu.cise.servicemodel.v1.service.Service;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.PullResponseBuilder;
import eu.eucise.helpers.ServiceBuilder;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LocationEntityTest {

    @Test
    void it_build_location_entity() throws IOException {

        String streamUri = "http://nodeB/service.nodeB?access_token=token&token_type=bearer&expires_in=3600";
        String xmlCapabilities = getXmlCapabilities();

        LocationDocumentEntityInformation locationEntitySent = new LocationDocumentEntityInformation(streamUri, xmlCapabilities);

        var pullResponse = PullResponseBuilder.newPullResponse()
                .creationDateTime(new Date())
                .correlationId("pullRequest.getCorrelationID()")
                .contextId("pullRequest.getContextID()")
                .generateId()
                .isRequiresAck(false)
                .priority(PriorityType.HIGH)
                .sender(buildService("serviceIdResponse"))
                .recipient(buildService("serviceIdResponse"))
                .resultCode(ResponseCodeType.SUCCESS)
                .informationSecurityLevel(InformationSecurityLevelType.NON_SPECIFIED)
                .informationSensitivity(InformationSensitivityType.NON_SPECIFIED)
                .purpose(PurposeType.NON_SPECIFIED)
                .isPersonalData(false)
                .addEntity(locationEntitySent.buildEntity())
                .build();


        LocationDocumentEntityInformation locationEntityReceived = new LocationDocumentEntityInformation(pullResponse);

        assertEquals(streamUri, locationEntityReceived.getStreamUri().get());
        assertEquals(xmlCapabilities, locationEntityReceived.getXmlCapabilities().get());
    }

    @Test
    void it_get_location_form_pull_response() throws IOException {
        PullResponse pullResponse = getPullResponse();

        LocationDocumentEntityInformation streamEntityReceived = new LocationDocumentEntityInformation(pullResponse);

        assertTrue(streamEntityReceived.getStreamUri().isPresent());
        assertTrue(streamEntityReceived.getXmlCapabilities().isPresent());

    }

    private String getXmlCapabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlShomCapabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }

    private PullResponse getPullResponse() throws IOException {
        XmlMapper xmlMapper = new DefaultXmlMapper();
        var resourcePath = new File("./target/test-classes/PullResponse_location_test.xml");
        var xml = new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
        return xmlMapper.fromXML(xml);
    }

    private Service buildService(String serviceId) {
        return ServiceBuilder.newService()
                .id(serviceId)
                .operation(ServiceOperationType.PULL)
                .type(ServiceType.LOCATION_DOCUMENT_SERVICE)
                .build();
    }
}