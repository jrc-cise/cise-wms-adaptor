package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.adaptor.wms.config.CiseConfig;
import eu.cise.adaptor.wms.helpers.CiseConfigLocationHelper;
import eu.cise.adaptor.wms.helpers.ProxyConfigHelper;
import eu.cise.datamodel.v1.entity.document.LocationDocument;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.ResponseCodeType;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.AdditionalMatchers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CapabilitiesResponseHandlerTest {

    private final ProxyConfigHelper proxyConfig = new ProxyConfigHelper();
    private final CiseConfig ciseConfig = new CiseConfigLocationHelper();

    SyncAsyncCoordinator syncAsyncCoordinator = mock(SyncAsyncCoordinator.class);
    ;
    AdaptorLogger logger = mock(AdaptorLogger.class);

    @Test
    void it_manage_a_pull_response_location_service() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());
        var response = getPullResponseTest();
        capabilitiesResponseHandler.accept(response);

        verify(syncAsyncCoordinator).giveResponse(eq(response.getCorrelationID()), eq(response.getResultCode()), AdditionalMatchers.not(eq(Optional.empty())));

    }

    @Test
    void it_manage_a_negative_pull_response_location_service() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());
        var response = getPullResponseTest();
        response.setResultCode(ResponseCodeType.END_POINT_NOT_FOUND);
        response.setErrorDetail("The capabilities are not available");
        capabilitiesResponseHandler.accept(response);

        verify(syncAsyncCoordinator).giveResponse(eq(response.getCorrelationID()), eq(response.getResultCode()), eq(Optional.empty()));
    }


    @Test
    void it_manage_no_xmlcapability_message() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());
        var response = getPullResponseTest();
        LocationDocument entity = (LocationDocument) ((XmlEntityPayload) response.getPayload()).getAnies().get(0);
        entity.getMetadatas().get(0).setFileSchema(null);
        capabilitiesResponseHandler.accept(response);

        verify(logger).logCiseResponseException(eq(response.getMessageID()), any());
        verify(syncAsyncCoordinator).giveResponse(eq(response.getCorrelationID()), eq(response.getResultCode()), eq(Optional.empty()));
    }

    @Test
    void it_gets_OnlineResource_tags_location_service_arcgis() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());

        String nl = capabilitiesResponseHandler.capabilityProxyzation(getXmlArcgisCapabilities(), Optional.of("http://nodeB/service.nodeB"));
        assertTrue(StringUtils.isNotBlank(nl));
        assertTrue(nl.startsWith("<WMS_Capabilities"));
    }

    @Test
    void it_gets_OnlineResource_tags_location_service_Inspire() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());

        String nl = capabilitiesResponseHandler.capabilityProxyzation(getXmlInspireCapabilities(), Optional.of("/GetMap/eu.green.proxyserver.document.pull.prov.maps"));
        assertTrue(StringUtils.isNotBlank(nl));
        assertTrue(nl.startsWith("<WMS_Capabilities"));
    }

    @Test
    void it_gets_OnlineResource_tags_location_service_Ncwms2() throws IOException {
        ServiceType serviceType = ServiceType.LOCATION_DOCUMENT_SERVICE;
        CapabilitiesResponseHandler capabilitiesResponseHandler = new CapabilitiesResponseHandler(serviceType, proxyConfig.nodeEndpointUrl(), syncAsyncCoordinator, logger, ciseConfig.wmsGetCapabilitiesUrl());

        String nl = capabilitiesResponseHandler.capabilityProxyzation(getXmlNcwms2Capabilities(), Optional.of("/GetMap/eu.green.proxyserver.document.pull.prov.maps"));
        assertTrue(StringUtils.isNotBlank(nl));
        assertTrue(nl.startsWith("<WMS_Capabilities"));
    }

    private PullResponse getPullResponseTest() throws IOException {
        XmlMapper xmlMapper = new DefaultXmlMapper();
        var resourcePath = new File("./target/test-classes/PullResponse_location_test.xml");
        var serviceXml = new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
        return xmlMapper.fromXML(serviceXml);
    }

    private String getXmlNcwms2Capabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlNcwms2Capabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }

    private String getXmlInspireCapabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlInspireCapabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }

    private String getXmlArcgisCapabilities() throws IOException {
        var resourcePath = new File("./target/test-classes/XmlArcgisCapabilities.xml");
        return new String(Files.readAllBytes(Path.of(resourcePath.getPath())));
    }
}