package eu.cise.adaptor.wms.domain.wmsclient;

import eu.cise.adaptor.wms.AdaptorLogger;
import eu.cise.servicemodel.v1.message.ResponseCodeType;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SyncAsyncCoordinatorTest {

    private final AdaptorLogger logger = Mockito.mock(AdaptorLogger.class);

    @Test
    void it_create_link_and_receive_response() {

        long timeout = 10;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId = "correlation-test";
        String xmlTest = "this is a test";

        var link = syncAsyncCoordinator.createLink(correlationId);

        syncAsyncCoordinator.giveResponse(correlationId, ResponseCodeType.SUCCESS , Optional.of(xmlTest));
        Response response = link.getResponse();

        assertEquals(200, response.getStatus());
        assertEquals(MediaType.APPLICATION_XML, response.getMediaType().toString());
        assertEquals(xmlTest, response.readEntity(String.class));
    }

    @Test
    void it_goes_timeout() {

        long timeout = 2;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId = "correlation-test";

        var link = syncAsyncCoordinator.createLink(correlationId);
        var response = link.getResponse();

        assertEquals(404, response.getStatus());
    }

    @Test
    void it_check_two_request_one_without_answer_goes_timeout() throws InterruptedException {

        long timeout = 2;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId1 = "correlation-test-1";
        String correlationId2 = "correlation-test-2";
        String xmlTest = "this is a test";

        var link1 = syncAsyncCoordinator.createLink(correlationId1);
        var link2 = syncAsyncCoordinator.createLink(correlationId2);

        AtomicInteger res1 = new AtomicInteger();
        AtomicInteger res2 = new AtomicInteger();
        ExecutorService executor = Executors.newFixedThreadPool(10);
        executor.execute(() -> res1.set(link1.getResponse().getStatus()));
        executor.execute(() -> res2.set(link2.getResponse().getStatus()));

        syncAsyncCoordinator.giveResponse(correlationId2, ResponseCodeType.SUCCESS, Optional.of(xmlTest));
        executor.awaitTermination(timeout + 1, TimeUnit.SECONDS);

        assertEquals(404, res1.get());
        assertEquals(200, res2.get());
    }

    @Test
    void it_create_response_401_for_no_auth_answer() {

        long timeout = 10;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId = "correlation-test";
        String xmlTest = "this is a test";

        var link = syncAsyncCoordinator.createLink(correlationId);

        syncAsyncCoordinator.giveResponse(correlationId, ResponseCodeType.AUTHENTICATION_ERROR , Optional.of(xmlTest));
        Response response = link.getResponse();

        assertEquals(401, response.getStatus());
    }

    @Test
    void it_create_response_404_for_empty_capabilities_answer() {

        long timeout = 10;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId = "correlation-test";
        String xmlTest = "this is a test";

        var link = syncAsyncCoordinator.createLink(correlationId);

        syncAsyncCoordinator.giveResponse(correlationId, ResponseCodeType.SUCCESS , Optional.empty());
        Response response = link.getResponse();

        assertEquals(404, response.getStatus());
    }

    @Test
    void it_create_response_503_for_empty_capabilities_answer() {

        long timeout = 10;
        SyncAsyncCoordinator syncAsyncCoordinator = new SyncAsyncCoordinator(timeout, logger);
        String correlationId = "correlation-test";
        String xmlTest = "this is a test";

        var link = syncAsyncCoordinator.createLink(correlationId);

        syncAsyncCoordinator.giveResponse(correlationId, ResponseCodeType.SERVER_ERROR , Optional.of(xmlTest));
        Response response = link.getResponse();

        assertEquals(503, response.getStatus());
    }
}