package eu.cise.adaptor.wms.helpers;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class TestHelpers {

    public static String readResource(String resourceName) {
        try {
            return Files.readString(Paths.get(getResourceURI(resourceName)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static URI getResourceURI(String resourceDir) {
        try {
            return Objects.requireNonNull(TestHelpers.class.getClassLoader().getResource(resourceDir)).toURI();
        } catch (URISyntaxException | NullPointerException e) {
            throw new RuntimeException(e);
        }
    }
}
