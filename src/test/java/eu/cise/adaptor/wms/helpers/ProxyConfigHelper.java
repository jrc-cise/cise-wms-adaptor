package eu.cise.adaptor.wms.helpers;

import eu.cise.adaptor.wms.config.GetMapConfig;

public class ProxyConfigHelper implements GetMapConfig {

    @Override
    public String nodeEndpointUrl() {
        return "http://proxy-address:7979";
    }
}
