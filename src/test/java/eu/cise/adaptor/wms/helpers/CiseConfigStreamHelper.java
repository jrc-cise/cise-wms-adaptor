package eu.cise.adaptor.wms.helpers;

import eu.cise.adaptor.wms.config.CiseConfig;
import eu.cise.servicemodel.v1.service.ServiceType;

public class CiseConfigStreamHelper implements CiseConfig {

    @Override
    public String nodeAddress() {
        return "https://cise-node/messages";
    }

    @Override
    public long waitCapabilitiesTimeout() {
        return 60;
    }

    @Override
    public String serviceIdRequest() {
        return "eu.green.shomclient.document.pull.cons.maps";
    }

    @Override
    public String serviceIdResponse() {
        return "eu.green.shomserver.document.pull.prov.maps";
    }

    @Override
    public ServiceType serviceType() {
        return ServiceType.DOCUMENT_SERVICE;
    }

    @Override
    public String wmsGetCapabilitiesUrl() {
        return "http://localhost:8080/wms";
    }

}
