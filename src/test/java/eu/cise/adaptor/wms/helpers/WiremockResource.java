package eu.cise.adaptor.wms.helpers;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.requestfilter.RequestFilterAction;
import com.github.tomakehurst.wiremock.extension.requestfilter.RequestWrapper;
import com.github.tomakehurst.wiremock.extension.requestfilter.StubRequestFilter;
import com.github.tomakehurst.wiremock.http.Body;
import com.github.tomakehurst.wiremock.http.ContentTypeHeader;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import eu.cise.servicemodel.v1.message.Message;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import static com.github.tomakehurst.wiremock.matching.RequestPatternBuilder.*;


import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonMap;

public class WiremockResource implements QuarkusTestResourceLifecycleManager {
    public static final int PORT_NUMBER = 8089;
    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        WireMockConfiguration options = new WireMockConfiguration();
        wireMockServer = new WireMockServer(options
                .port(PORT_NUMBER)
                .bindAddress("localhost"));
      //          .extensions(new SignatureRequestFilter()));
        wireMockServer.start();

        wireMockServer.stubFor(get(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", equalTo("application/xml"))
                .andMatching(value -> {
                    String maybeHeaderProperty = value.getHeader("USER");
                    if (maybeHeaderProperty == null || maybeHeaderProperty.equals("testUser")) {
                        return MatchResult.exactMatch();
                    }
                    return MatchResult.noMatch();
                })
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(getCapabilitiesTest())));


        wireMockServer.stubFor(post(urlEqualTo("/ncwms2/wms/wrongauth"))
                .withHeader("Accept", equalTo("application/xml"))
                .willReturn(aResponse()
                        .withStatus(405)));
/*
        wireMockServer.stubFor(post(urlEqualTo("/api/http505"))
                .withHeader("Accept", equalTo("application/xml"))
                .willReturn(aResponse()
                        .withStatus(505)));
*/

        return singletonMap("wiremock.baseUrl", wireMockServer.baseUrl());
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }

 //   @NotNull
    private String syncAck() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<ns4:Acknowledgement xmlns:ns2=\"http://www.cise.eu/servicemodel/v1/authority/\" xmlns:ns4=\"http://www.cise.eu/servicemodel/v1/message/\" xmlns:ns3=\"http://www.cise.eu/servicemodel/v1/service/\">\n" +
                "    <CorrelationID>d8a003e2-9b0a-4b09-9d2b-94ed87f92c1f_d62b0e1f-a9d2-4333-92ef-0e4aaaf012f2</CorrelationID>\n" +
                "    <CreationDateTime>2019-08-22T13:58:43.923Z</CreationDateTime>\n" +
                "    <MessageID>01d19315-de7e-4709-840c-3078a92fe1ca</MessageID>\n" +
                "    <Priority>High</Priority>\n" +
                "    <RequiresAck>false</RequiresAck>\n" +
                "    <AckCode>Success</AckCode>\n" +
                "    <AckDetail>Message delivered to all 1 recipients</AckDetail>\n" +
                "    <DiscoveredServices>\n" +
                "        <ServiceID>cx.simlsa2-nodecx.vessel.subscribe.consumer</ServiceID>\n" +
                "    </DiscoveredServices>\n" +
                "</ns4:Acknowledgement>";
    }

    public static class SignatureRequestFilter extends StubRequestFilter {

        private final XmlMapper xmlMapper = new DefaultXmlMapper.NotValidating();

        @Override
        public RequestFilterAction filter(Request request) {

            return RequestFilterAction.continueWith(RequestWrapper.create().transformBody(
                    b -> {
                        Message message = xmlMapper.fromXML(b.asString());
                        message.setAny(null);
                        return Body.ofBinaryOrText(xmlMapper.toXML(message).getBytes(UTF_8), ContentTypeHeader.absent());
                    }).wrap(request));
        }

        @Override
        public String getName() {
            return "signature-filter";
        }
    }

    /**
     * This Capabilities xml has:
     * - DOCTYPE tag
     * - OnlineResource href: http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer
     * - 9 OnlineResource tags
     *
     * @return
     */
    private String getCapabilitiesTest() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE WMS_Capabilities SYSTEM \"http://jump1.ednode02.cise.eu/getcapabilitiesmock/WMS_MS_Capabilities.dtd\">" +
                "<WMS_Capabilities version=\"1.3.0\"\n" +
                "  xmlns=\"http://www.opengis.net/wms\"\n" +
                "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "  xmlns:esri_wms=\"http://www.esri.com/wms\"\n" +
                "  xsi:schemaLocation=\"http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd http://www.esri.com/wms http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?version=1.3.0&amp;service=WMS&amp;request=GetSchemaExtension\">\n" +
                "  <Service>\n" +
                "    <Name><![CDATA[WMS]]></Name>\n" +
                "    <Title><![CDATA[Specialty/ESRI_StatesCitiesRivers_USA]]></Title>\n" +
                "    <Abstract>WMS</Abstract>\n" +
                "    <KeywordList><Keyword><![CDATA[]]></Keyword></KeywordList>\n" +
                "    <OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/>\n" +
                "    <ContactInformation>\n" +
                "      <ContactPersonPrimary>\n" +
                "        <ContactPerson><![CDATA[]]></ContactPerson>\n" +
                "        <ContactOrganization><![CDATA[]]></ContactOrganization>\n" +
                "      </ContactPersonPrimary>\n" +
                "      <ContactPosition><![CDATA[]]></ContactPosition>\n" +
                "      <ContactAddress>\n" +
                "        <AddressType><![CDATA[]]></AddressType>\n" +
                "        <Address><![CDATA[]]></Address>\n" +
                "        <City><![CDATA[]]></City>\n" +
                "        <StateOrProvince><![CDATA[]]></StateOrProvince>\n" +
                "        <PostCode><![CDATA[]]></PostCode>\n" +
                "        <Country><![CDATA[]]></Country>\n" +
                "      </ContactAddress>\n" +
                "      <ContactVoiceTelephone><![CDATA[]]></ContactVoiceTelephone>\n" +
                "      <ContactFacsimileTelephone><![CDATA[]]></ContactFacsimileTelephone>\n" +
                "      <ContactElectronicMailAddress><![CDATA[]]></ContactElectronicMailAddress>\n" +
                "    </ContactInformation>\n" +
                "    <Fees><![CDATA[]]></Fees>\n" +
                "    <AccessConstraints><![CDATA[]]></AccessConstraints>\n" +
                "    <MaxWidth>2048</MaxWidth>\n" +
                "    <MaxHeight>2048</MaxHeight>\n" +
                "  </Service>\n" +
                "  <Capability>\n" +
                "    <Request>\n" +
                "      <GetCapabilities>\n" +
                "        <Format>application/vnd.ogc.wms_xml</Format>\n" +
                "        <Format>text/xml</Format>\n" +
                "        <DCPType>\n" +
                "          <HTTP>" +
                "               <Get><OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/></Get>" +
                "               <Post><OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/></Post>" +
                "           </HTTP>\n" +
                "        </DCPType>\n" +
                "      </GetCapabilities>\n" +
                "      <GetMap>\n" +
                "        <Format>image/bmp</Format>\n" +
                "        <Format>image/jpeg</Format>\n" +
                "        <Format>image/tiff</Format>\n" +
                "        <Format>image/png</Format>\n" +
                "        <Format>image/png8</Format>\n" +
                "        <Format>image/png24</Format>\n" +
                "        <Format>image/png32</Format>\n" +
                "        <Format>image/gif</Format>\n" +
                "        <Format>image/svg+xml</Format>\n" +
                "        <DCPType>\n" +
                "          <HTTP><Get><OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/></Get></HTTP>\n" +
                "        </DCPType>\n" +
                "      </GetMap>\n" +
                "      <GetFeatureInfo>\n" +
                "        <Format>application/vnd.esri.wms_raw_xml</Format>\n" +
                "        <Format>application/vnd.esri.wms_featureinfo_xml</Format>\n" +
                "        <Format>application/vnd.ogc.wms_xml</Format>\n" +
                "        <Format>text/xml</Format>\n" +
                "        <Format>text/html</Format>\n" +
                "        <Format>text/plain</Format>\n" +
                "        <DCPType>\n" +
                "          <HTTP><Get><OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/></Get></HTTP>\n" +
                "        </DCPType>\n" +
                "      </GetFeatureInfo>\n" +
                "      <esri_wms:GetStyles>\n" +
                "        <Format>application/vnd.ogc.sld+xml</Format>\n" +
                "        <DCPType>\n" +
                "          <HTTP><Get><OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:type=\"simple\" xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer\"/></Get></HTTP>\n" +
                "        </DCPType>\n" +
                "      </esri_wms:GetStyles>\n" +
                "    </Request>\n" +
                "    <Exception>\n" +
                "      <Format>application/vnd.ogc.se_xml</Format>\n" +
                "      <Format>application/vnd.ogc.se_inimage</Format>\n" +
                "      <Format>application/vnd.ogc.se_blank</Format>\n" +
                "      <Format>text/xml</Format>\n" +
                "      <Format>XML</Format>\n" +
                "    </Exception>\n" +
                "    <Layer>\n" +
                "      <Title><![CDATA[Layers]]></Title>\n" +
                "<CRS>CRS:84</CRS>\n" +
                "<CRS>EPSG:4326</CRS>\n" +
                "<EX_GeographicBoundingBox><westBoundLongitude>-125.192865</westBoundLongitude><eastBoundLongitude>-66.105824</eastBoundLongitude><southBoundLatitude>19.416377</southBoundLatitude><northBoundLatitude>54.318281</northBoundLatitude></EX_GeographicBoundingBox>\n" +
                "<BoundingBox CRS=\"CRS:84\" minx=\"-125.192865\" miny=\"19.416377\" maxx=\"-66.105824\" maxy=\"54.318281\"/>\n" +
                "<BoundingBox CRS=\"EPSG:4326\" minx=\"19.416377\" miny=\"-125.192865\" maxx=\"54.318281\" maxy=\"-66.105824\"/>\n" +
                "      <Layer queryable=\"1\">\n" +
                "        <Name>0</Name>\n" +
                "        <Title><![CDATA[States]]></Title>\n" +
                "        <Abstract><![CDATA[States]]></Abstract>\n" +
                "<CRS>CRS:84</CRS>\n" +
                "<CRS>EPSG:4326</CRS>\n" +
                "<EX_GeographicBoundingBox><westBoundLongitude>-178.217598</westBoundLongitude><eastBoundLongitude>-66.969271</eastBoundLongitude><southBoundLatitude>18.924782</southBoundLatitude><northBoundLatitude>71.406235</northBoundLatitude></EX_GeographicBoundingBox>\n" +
                "<BoundingBox CRS=\"CRS:84\" minx=\"-178.217598\" miny=\"18.924782\" maxx=\"-66.969271\" maxy=\"71.406235\"/>\n" +
                "<BoundingBox CRS=\"EPSG:4326\" minx=\"18.924782\" miny=\"-178.217598\" maxx=\"71.406235\" maxy=\"-66.969271\"/>\n" +
                "        <Style>\n" +
                "          <Name>default</Name>\n" +
                "          <Title>0</Title>\n" +
                "          <LegendURL width=\"76\" height=\"16\">\n" +
                "            <Format>image/png</Format>\n" +
                "            <OnlineResource xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=0\" xlink:type=\"simple\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" />\n" +
                "          </LegendURL>\n" +
                "        </Style>\n" +
                "      </Layer>\n" +
                "      <Layer queryable=\"1\">\n" +
                "        <Name>1</Name>\n" +
                "        <Title><![CDATA[Rivers]]></Title>\n" +
                "        <Abstract><![CDATA[Rivers]]></Abstract>\n" +
                "<CRS>CRS:84</CRS>\n" +
                "<CRS>EPSG:4326</CRS>\n" +
                "<EX_GeographicBoundingBox><westBoundLongitude>-164.765831</westBoundLongitude><eastBoundLongitude>-67.790980</eastBoundLongitude><southBoundLatitude>25.845557</southBoundLatitude><northBoundLatitude>70.409756</northBoundLatitude></EX_GeographicBoundingBox>\n" +
                "<BoundingBox CRS=\"CRS:84\" minx=\"-164.765831\" miny=\"25.845557\" maxx=\"-67.790980\" maxy=\"70.409756\"/>\n" +
                "<BoundingBox CRS=\"EPSG:4326\" minx=\"25.845557\" miny=\"-164.765831\" maxx=\"70.409756\" maxy=\"-67.790980\"/>\n" +
                "        <Style>\n" +
                "          <Name>default</Name>\n" +
                "          <Title>1</Title>\n" +
                "          <LegendURL width=\"76\" height=\"16\">\n" +
                "            <Format>image/png</Format>\n" +
                "            <OnlineResource xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=1\" xlink:type=\"simple\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" />\n" +
                "          </LegendURL>\n" +
                "        </Style>\n" +
                "      </Layer>\n" +
                "      <Layer queryable=\"1\">\n" +
                "        <Name>2</Name>\n" +
                "        <Title><![CDATA[Cities]]></Title>\n" +
                "        <Abstract><![CDATA[Cities]]></Abstract>\n" +
                "<CRS>CRS:84</CRS>\n" +
                "<CRS>EPSG:4326</CRS>\n" +
                "<EX_GeographicBoundingBox><westBoundLongitude>-158.064606</westBoundLongitude><eastBoundLongitude>-67.986769</eastBoundLongitude><southBoundLatitude>19.696150</southBoundLatitude><northBoundLatitude>64.869104</northBoundLatitude></EX_GeographicBoundingBox>\n" +
                "<BoundingBox CRS=\"CRS:84\" minx=\"-158.064606\" miny=\"19.696150\" maxx=\"-67.986769\" maxy=\"64.869104\"/>\n" +
                "<BoundingBox CRS=\"EPSG:4326\" minx=\"19.696150\" miny=\"-158.064606\" maxx=\"64.869104\" maxy=\"-67.986769\"/>\n" +
                "        <Style>\n" +
                "          <Name>default</Name>\n" +
                "          <Title>2</Title>\n" +
                "          <LegendURL width=\"72\" height=\"16\">\n" +
                "            <Format>image/png</Format>\n" +
                "            <OnlineResource xlink:href=\"http://sampleserver1b.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=2\" xlink:type=\"simple\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" />\n" +
                "          </LegendURL>\n" +
                "        </Style>\n" +
                "      </Layer>\n" +
                "    </Layer>\n" +
                "  </Capability>\n" +
                "</WMS_Capabilities>\n";
    }

}
