package eu.cise.adaptor.wms;

import eu.cise.adaptor.wms.security.crypto.EncryptorAesGcmPassword;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EncryptFileTest {

    private static final String cred1 = "user1:password1";
    private static final String cred2 = "user2:password2";
    private static final String cred3 = "user3:password3";

    private static final String TEST_FILE_NAME = "to_encript.txt";

    @BeforeAll
    static void createTesFile() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(TEST_FILE_NAME));
        writer.write(cred1);
        writer.newLine();
        writer.write(cred2);
        writer.newLine();
        writer.write(cred3);
        writer.newLine();
        writer.close();
    }

    @AfterAll
    static void deleteTestFile() {
        File testFile = new File(TEST_FILE_NAME);
        testFile.delete();
    }

    @Test
    void it_create_file() throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, InvalidKeyException {

        EncryptFile.createFile(TEST_FILE_NAME);

        String[] credentials = new String[]{cred1, cred2, cred3};
        int idx = 0;
        try (var scanner = new Scanner(new FileInputStream(TEST_FILE_NAME), StandardCharsets.UTF_8).useDelimiter("\n")) {
            while (scanner.hasNext()) {
                var line = scanner.next();
                var decoded = EncryptorAesGcmPassword.decrypt(line);
                assertEquals(credentials[idx++], decoded);
            }
        }
    }
}