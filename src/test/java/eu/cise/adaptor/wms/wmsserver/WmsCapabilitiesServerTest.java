package eu.cise.adaptor.wms.wmsserver;

import com.github.tomakehurst.wiremock.client.BasicCredentials;
import eu.cise.adaptor.wms.helpers.WiremockResource;
import eu.cise.adaptor.wms.security.BasicAuthFileWmsCredentialRepository;
import eu.cise.adaptor.wms.security.ServerWmsCredentialRepository;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.awaitility.Awaitility.waitAtMost;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@QuarkusTest
@QuarkusTestResource(WiremockResource.class)
class WmsCapabilitiesServerTest {

    private final ServerWmsCredentialRepository wmsRepository = new BasicAuthFileWmsCredentialRepository("./target/test-classes/wmscredentials.txt");

    @Inject
    @RestClient
    WmsClient wmsClient;

    @BeforeAll
    static void beforeAll() {
        configureFor(WiremockResource.PORT_NUMBER);
    }

    @Test
    void it_test_connectivity() {

        WmsCapabilitiesServer wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient, wmsRepository);

        String result = wmsCapabilitiesServer.getCapabilities();
        assertNotNull(result);

    }

    @Test
    void it_test_exception() {

        String errMessage = "this is a test";
        WebApplicationException except = new WebApplicationException(errMessage);
        WmsClient wmsClient = Mockito.mock(WmsClient.class);
        doThrow(except).when(wmsClient).getCapabilities(any());
        WmsCapabilitiesServer wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient, wmsRepository);

        assertThrows(WmsServerException.class, wmsCapabilitiesServer::getCapabilities);

    }

    @Test
    void it_test_getCapabilities_with_basic_auth() {

        WmsCapabilitiesServer wmsCapabilitiesServer = new WmsCapabilitiesServer(wmsClient, wmsRepository);
        var cr = wmsRepository.getWmsCredential();
        BasicCredentials basicCredentials = new BasicCredentials(cr.getLeft(), cr.getRight());
        var result = wmsCapabilitiesServer.getCapabilities();

        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withBasicAuth(basicCredentials)
        ));

        assertNotNull(result);
    }
}