package eu.cise.adaptor.wms.resources;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import static io.restassured.RestAssured.given;

@QuarkusTest
class WmsClientResourceTest {

    @Test
    void it_sends_request_without_basic_authorization_and_is_refused_GET() {
        given()
                .when().get("/wms")
                .then().statusCode(401);
    }

    @Test
    void it_sends_request_without_basic_authorization_and_is_refused_POST() {
        given()
                .when().post("/wms")
                .then().statusCode(401);
    }

    @Test
    void it_sends_request_with_wrong_basic_authorization_and_is_refused_GET() {

        String username = "wronguser";
        String password = "wrongpassword";
        String authString = username + ":" + password;

        given()
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8)))
                .when().get("/wms")
                .then().statusCode(401);
    }

    @Test
    void it_sends_request_with_wrong_basic_authorization_and_is_refused_POST() {

        String username = "wronguser";
        String password = "wrongpassword";
        String authString = username + ":" + password;

        given()
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8)))
                .when().post("/wms")
                .then().statusCode(401);
    }

    @Test
    void it_sends_request_with_right_basic_authorization_and_is_accepted_GET() {

        String username = "user1";
        String password = "password1";
        String authString = username + ":" + password;

        given()
                .header("Authorization", "Basic " +Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8)))
                .when().get("/wms")
                .then()
                .statusCode(404);
    }

    @Test
    void it_sends_request_with_right_basic_authorization_and_is_accepted_POST() {

        String username = "user2";
        String password = "password2";
        String authString = username + ":" + password;

        given()
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8)))
                .when().post("/wms")
                .then()
                .statusCode(404);
    }

    @Test
    void it_sends_request_with_empty_basic_authorization_and_is_refused_GET() {
        
        given()
                .header("Authorization", "Basic ")
                .when().get("/wms")
                .then()
                .statusCode(401);
    }
    @Test
    void it_sends_request_with_empty_basic_authorization_and_is_refused_POST() {
        
        given()
                .header("Authorization", "Basic ")
                .when().post("/wms")
                .then()
                .statusCode(401);
    }

}