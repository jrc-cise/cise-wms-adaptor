package eu.cise.adaptor.wms.resources;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@QuarkusTest
class MessageResourceTest {

    @Test
    void it_check_permit_all() {
        given()
                .when()
                .accept(APPLICATION_XML)
                .contentType(APPLICATION_XML)
                .post("/rest/api/messages")
                .then().statusCode(200);
    }
}