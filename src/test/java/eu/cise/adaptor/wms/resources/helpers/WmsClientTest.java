package eu.cise.adaptor.wms.resources.helpers;

import com.github.tomakehurst.wiremock.client.BasicCredentials;
import eu.cise.adaptor.wms.helpers.WiremockResource;
import eu.cise.adaptor.wms.wmsserver.AddCustomHeaderParameter;
import eu.cise.adaptor.wms.wmsserver.WmsClient;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.awaitility.Awaitility.waitAtMost;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@QuarkusTest
@QuarkusTestResource(WiremockResource.class)
class WmsClientTest {

    @Inject
    @RestClient
    WmsClient wmsClient;

    @BeforeAll
    static void beforeAll() {
        configureFor(WiremockResource.PORT_NUMBER);
    }

    @Test
    void it_test_getCapabilities_no_basic_auth() {

        var result = wmsClient.getCapabilities(new WmsClientParams());

        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
        ));

        assertNotNull(result);
    }

    @Test
    void it_test_getCapabilities_with_basic_auth() throws InterruptedException {

        BasicCredentials basicCredentials = new BasicCredentials("username", "password");
        var result = wmsClient.getCapabilities(new WmsClientParams(basicCredentials.username, basicCredentials.password));

        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withBasicAuth(basicCredentials)
        ));

        assertNotNull(result);
    }


    @Test
    void it_test_getCapabilities_with_custom_headerParam() {

        var result = wmsClient.getCapabilities(new WmsClientParams());

        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withHeader("USER", equalTo("testUser"))
        ));

        assertNotNull(result);
    }

    @Test
    void it_test_getCapabilities_without_custom_headerParam_with_both_header_params() {

        // given
        ConfigProviderResolver orginalResolver = ConfigProviderResolver.instance();
        Config mockConfig = mock(Config.class);
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-key", String.class)).thenReturn(Optional.of(""));
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-value", String.class)).thenReturn(Optional.of(""));
        ConfigProviderResolver mockResolver = mock(ConfigProviderResolver.class);
        when(mockResolver.getConfig()).thenReturn(mockConfig);
        ConfigProviderResolver.setInstance(mockResolver);

        //when
        var result = wmsClient.getCapabilities(new WmsClientParams());

        //then
        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withoutHeader("USER"))
        );
        assertNotNull(result);
        ConfigProviderResolver.setInstance(orginalResolver);
    }

    @Test
    void it_test_getCapabilities_without_custom_headerParam_with_only_headerParamKey() {

        // given
        ConfigProviderResolver originInstance = ConfigProviderResolver.instance();
        Config mockConfig = mock(Config.class);
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-key", String.class)).thenReturn(Optional.of(""));
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-value", String.class)).thenReturn(Optional.empty());
        ConfigProviderResolver mockResolver = mock(ConfigProviderResolver.class);
        when(mockResolver.getConfig()).thenReturn(mockConfig);
        ConfigProviderResolver.setInstance(mockResolver);

        //when
        var result = wmsClient.getCapabilities(new WmsClientParams());

        //then
        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withoutHeader("USER"))
        );
        assertNotNull(result);
        ConfigProviderResolver.setInstance(originInstance);
    }

    @Test
    void it_test_getCapabilities_without_custom_headerParam_with_only_headerParamValue() {

        // given
        ConfigProviderResolver originInstance = ConfigProviderResolver.instance();
        Config mockConfig = mock(Config.class);
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-key", String.class)).thenReturn(Optional.empty());
        when(mockConfig.getOptionalValue("wms-api/mp-rest/header-param-value", String.class)).thenReturn(Optional.of(""));
        ConfigProviderResolver mockResolver = mock(ConfigProviderResolver.class);
        when(mockResolver.getConfig()).thenReturn(mockConfig);
        ConfigProviderResolver.setInstance(mockResolver);

        //when
        var result = wmsClient.getCapabilities(new WmsClientParams());

        //then
        waitAtMost(3, TimeUnit.SECONDS).untilAsserted(() -> verify(getRequestedFor(urlPathEqualTo("/ncwms2/wms"))
                .withHeader("Accept", containing("application/xml"))
                .withQueryParam("service", equalTo("WMS"))
                .withQueryParam("request", equalTo("GetCapabilities"))
                .withQueryParam("version", equalTo("1.3.0"))
                .withoutHeader("USER"))
        );
        assertNotNull(result);
        ConfigProviderResolver.setInstance(originInstance);
    }

}