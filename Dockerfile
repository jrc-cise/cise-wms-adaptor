FROM azul/zulu-openjdk-alpine:11-jre
LABEL maintainer="JRC-CISE-DEV@ec.europa.eu"

RUN apk update &&\
    apk upgrade &&\
    apk add bash

ARG VERSION=2.1.3

COPY target/cise-wms-adaptor-bin-"$VERSION"".tar.gz" /srv/cise-wms-adaptor.tar.gz
RUN mkdir -p /srv/cise-wms-adaptor && tar xvfz /srv/cise-wms-adaptor.tar.gz -C /srv/cise-wms-adaptor --strip-components 1
RUN rm /srv/cise-wms-adaptor.tar.gz

# Create a group and user. NB: this user group should exists in the host machine and the volume must be writeble by it
#RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# All future commands should run as the appuser user
#USER appuser

EXPOSE 8080

WORKDIR /srv/cise-wms-adaptor

VOLUME /srv/cise-wms-adaptor/config /srv/cise-wms-adaptor/logs

ENTRYPOINT ["/srv/cise-wms-adaptor/adaptor"]
CMD ["run"]
